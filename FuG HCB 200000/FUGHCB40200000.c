/******************************************************************************
* @author E.Chanel J.Thorne
*********************************************************************************/

#include "FUGHCB40200000.h"

static int mode_status;

int FUGHCB40200000_error_handle(int error_code, char* err_msg)
{
	char error_prefix[] = "FUGHCB: "; //define prefix of the return for the error
	switch(error_code)
	{
		case 0 :
			break;
		case 1 :
			sprintf(err_msg, "%sNO DATA AVAILABLE\n",error_prefix);
			break;
		case 2 :
			sprintf(err_msg, "%sUNKNOWN REGISTER TYPE\n",error_prefix);
			break;
		case 4 :
			sprintf(err_msg, "%sINVALID ARGUMENT\n",error_prefix);
			break;
		case 5 :
			sprintf(err_msg, "%sARGUMENT OUT OF RANGE\n",error_prefix);
			break;
		case 6 :
			sprintf(err_msg, "%sREGISTER IS READ-ONLY\n",error_prefix);
			break;
		case 7 :
			sprintf(err_msg, "%sRECEIVE OVERFLOW\n",error_prefix);
			break;
		case 8 :
			sprintf(err_msg, "%sEEPROM IS WRITE PROTECTED\n",error_prefix);
			break;
		case 9 :
			sprintf(err_msg, "%sADDRESS ERROR\n",error_prefix);
			break;
		case 10 :
			sprintf(err_msg, "%sUNKNOWN COMMAND\n",error_prefix);
			break;
		case 11 :
			sprintf(err_msg, "%sNOT ALLOWED TRIGGER-ON-TALK\n",error_prefix);
			break;
		case 12 :
			sprintf(err_msg, "%sINVALID ARGUMENT IN ~Tn COMMAND\n",error_prefix);
			break;
		case 13 :
			sprintf(err_msg, "%sINVALID N-VALUE\n",error_prefix);
			break;
		case 14 :
			sprintf(err_msg, "%sREGISTER IS WRITE ONLY\n",error_prefix);
			break;
		case 15 :
			sprintf(err_msg, "%sSTRING TOO LONG\n",error_prefix);
			break;
		case 16 :
			sprintf(err_msg, "%sWRONG CHECKSUM\n",error_prefix);
			break;
		case 17 :
			sprintf(err_msg, "%sINTERLOCK OPEN?\n",error_prefix);
			break;
		case 18 :
			sprintf(err_msg, "%sDIGITAL PROGRAMMING NOT ACTIVE\n",error_prefix);
			break;
		case 19 :
			sprintf(err_msg, "%sERROR WRITING TO THE DEVICE\n",error_prefix);
			break;
		default :
			sprintf(err_msg, "%sUNKNOWN ERROR CODE\n",error_prefix);
	}
	return FUGHCB_SUCCESS;
} 
//list of codes and meanings

int FUGHCB40200000_connect(const char* port_id, char* error)
{
    memset(error, '\0', FUGHCB_MAX_MSG_SIZE* sizeof(char));
	int serial_port = open(port_id, O_RDWR | O_NOCTTY | O_SYNC);
	if(serial_port == -1)                        // Error Checking 
	{
		sprintf(error, "FUGHCB: ERROR OPENING DEVICE");
		return FUGHCB_CONNECT_FAIL;
	}
	else
	{
        if(FUGHCB40200000_configure(serial_port, error))
        {
            tcflush(serial_port,TCIOFLUSH);
            return FUGHCB_CONNECT_FAIL;
        }
	}
	return serial_port;
}

int FUGHCB40200000_configure(const int serial_port, char* error){
		   	
	struct termios options;
	memset(&options, 0, sizeof(options));  /* clear the new struct */
    memset(error, 0, FUGHCB_MAX_MSG_SIZE*sizeof(char));

	cfsetispeed(&options, B115200); //baud rate
	cfsetospeed(&options, B115200);
	options.c_iflag &= ~(IGNBRK | BRKINT | ICRNL | INLCR | PARMRK | INPCK | ISTRIP | IXON);
	options.c_oflag = 0;
	options.c_lflag &= ~(ECHO | ECHONL | IEXTEN );
	options.c_lflag |= ICANON | ISIG;
	options.c_cflag &= ~(CSIZE | PARENB);
	options.c_cflag |= CS8;
   	options.c_cflag |= CLOCAL | CREAD;

	options.c_cc[VMIN]  = 0; 
 	options.c_cc[VTIME] = 0;
	options.c_cc[VEOL] = 0;
	options.c_cc[VEOL2] = 0;
	options.c_cc[VEOF] = 0x04;

	
	if(tcsetattr(serial_port, TCSANOW, &options) != 0)
  	{
		sprintf(error, "FUGHCB: FAILURE TO CONFIGURE TERMIOS");
		return FUGHCB_FALIURE;
  	}
	else
	{
		return FUGHCB_SUCCESS;
	}
}

int FUGHCB40200000_write(const int serial_port, char* msg, char* error)
{
	unsigned int status = 0x00;
	ioctl(serial_port, TIOCMGET, &status);//define type of message
    memset(error, '\0', FUGHCB_MAX_MSG_SIZE);
	int nbytes_written = write(serial_port, msg, strlen(msg)+1);//builds the message to be sent
	
	if(nbytes_written != strlen(msg)+1)
		{FUGHCB40200000_error_handle(19, error); printf("write errno error: %d",errno);
		return FUGHCB_FALIURE;} //error check
	else
		{return FUGHCB_SUCCESS;}
}

int FUGHCB40200000_read(const int serial_port, char* reply, char* error)
{
	int retval = 0;
	memset(reply, '\0', FUGHCB_MAX_MSG_SIZE* sizeof(char));

	int bytes_available = 0;

// OLD SHIT
    long msec = 0, trigger = 5000; // 5s to wait before timeout
    clock_t before = clock();
    while(bytes_available<1)
    {
        clock_t difference = clock() - before;
        msec = difference * 1000 / CLOCKS_PER_SEC; //50-60 msec delay for communication
        ioctl(serial_port, FIONREAD, &bytes_available);
        if(msec > trigger)
        {
            sprintf(error, "FUGHCB: READ TIMEOUT\n");
            return FUGHCB_FALIURE;
        }
    }




	//FD_ZERO(&readfds_FUGHCB);
	//FD_SET(serial_port, &readfds_FUGHCB);

	//timeout_FUGHCB.tv_sec = 1; //1 second timeout
	//timeout_FUGHCB.tv_usec = 0; 

	//retval = select(serial_port+1, &readfds_FUGHCB, NULL, NULL, &timeout_FUGHCB);
/*
	if(retval == -1){
		sprintf(error, "FUGHCB select() errno: %d", errno);
	}
	else if(retval == 0)
	{
		sprintf(error, "FUGHCB: READ TIMEOUT!");
        return FUGHCB_FALIURE;
	}
*/
    read(serial_port, reply, FUGHCB_MAX_MSG_SIZE);

	if(reply[0] == 'E' && reply[1] != '0')//if message is accepted, will return E0. However, some queries can reply with M1:## so this is taken into account. Any error from the PS will return E1-16. 
	{
		int ret_error_code;
		unsigned int msg_length = strlen(reply);
		char char_error_code[msg_length];
		memcpy(char_error_code, reply+1, msg_length);
		sscanf(char_error_code, "%d", &ret_error_code);
        FUGHCB40200000_error_handle(ret_error_code, error);
		return FUGHCB_FALIURE;
	}	
	else
	{return FUGHCB_SUCCESS;}
}

int FUGHCB40200000_disconnect(const int serial_port)
{
	return close(serial_port);
}

int FUGHCB40200000_set_reset(int serial_port, char* error)
{
	char reply[FUGHCB_MAX_MSG_SIZE];
	char error_msg[FUGHCB_MAX_MSG_SIZE];
    memset(error, '\0', FUGHCB_MAX_MSG_SIZE *sizeof(char));

    if(FUGHCB40200000_write(serial_port, "= \r\n", error_msg)){sprintf(error,"%s",error_msg);return FUGHCB_FALIURE;}

    if(FUGHCB40200000_read(serial_port, reply, error_msg)){sprintf(error,"%s",error_msg);return FUGHCB_FALIURE;}

	return FUGHCB_SUCCESS;
}

int FUGHCB40200000_get_identify(const int serial_port, char* reply, char* error)
{
    char error_msg[FUGHCB_MAX_MSG_SIZE];
    memset(reply, '\0', FUGHCB_MAX_MSG_SIZE* sizeof(char));
    memset(error, '\0', FUGHCB_MAX_MSG_SIZE* sizeof(char));

    if(FUGHCB40200000_write(serial_port, "*IDN? \r\n", error_msg)){sprintf(error,"%s",error_msg);return FUGHCB_FALIURE;}

    if(FUGHCB40200000_read(serial_port, reply, error_msg)){sprintf(error,"%s",error_msg);return FUGHCB_FALIURE;}

    return FUGHCB_SUCCESS;
}

int FUGHCB40200000_get_SELD(const int serial_port, char* status, char* error)
{
	char reply[FUGHCB_MAX_MSG_SIZE];
    char error_msg[FUGHCB_MAX_MSG_SIZE];
    memset(error, '\0', FUGHCB_MAX_MSG_SIZE* sizeof(char));
    memset(status, '\0', FUGHCB_MAX_MSG_SIZE* sizeof(char));

    if(FUGHCB40200000_write(serial_port, ">DSD? \r\n", error_msg)){sprintf(error,"%s",error_msg);return FUGHCB_FALIURE;}

    if(FUGHCB40200000_read(serial_port, reply, error_msg)){sprintf(error,"%s",error_msg);return FUGHCB_FALIURE;}

	if(reply[0] == 'D' && reply[1] == 'S' && reply[2] == 'D')
	{
		if(reply[4] != '1'){FUGHCB40200000_error_handle(18, error);return FUGHCB_FALIURE;}//will return special error if there is an issue
		else{sprintf(status, "FUGHCB: Digital control active!");}
	}
	else{sprintf(status, '\0'); sprintf(error,"READ ERROR");return FUGHCB_FALIURE;}
	return FUGHCB_SUCCESS;
}
//each function needs a read otherwise the message will be returned in the next use of read funtion
int FUGHCB40200000_set_interlock(const int serial_port, int status, char* error)
{
	char msg[FUGHCB_MAX_MSG_SIZE];
	char reply[FUGHCB_MAX_MSG_SIZE];
	char error_msg[FUGHCB_MAX_MSG_SIZE];
	memset(error,'\0', FUGHCB_MAX_MSG_SIZE);

	sprintf(msg, ">B2 %i \r\n", status); //B2 command queries the interlock system

    if(FUGHCB40200000_write(serial_port, msg, error_msg)){sprintf(error,"%s",error_msg);return FUGHCB_FALIURE;}

    if(FUGHCB40200000_read(serial_port, reply, error_msg)){sprintf(error,"%s",error_msg);return FUGHCB_FALIURE;}

	return FUGHCB_SUCCESS;
}

int FUGHCB40200000_set_HV_output(const int serial_port, int status, char* error)
{
	char msg[FUGHCB_MAX_MSG_SIZE];
	char reply[FUGHCB_MAX_MSG_SIZE];
	char error_msg[FUGHCB_MAX_MSG_SIZE];
	memset(error,'\0', FUGHCB_MAX_MSG_SIZE);
	sprintf(msg, ">BON %i \r\n", status); //BON command has to be set with B2. BON turns the output on and off.

    if(FUGHCB40200000_write(serial_port, msg, error_msg)){sprintf(error,"%s",error_msg);return FUGHCB_FALIURE;}//status = 0 is off

    if(FUGHCB40200000_read(serial_port, reply, error_msg)){sprintf(error,"%s",error_msg);return FUGHCB_FALIURE;}

    return FUGHCB_SUCCESS;
}

int FUGHCB40200000_get_output_state(const int serial_port, int* status, char* error)
{
	char reply[FUGHCB_MAX_MSG_SIZE];
    char error_msg[FUGHCB_MAX_MSG_SIZE];
    memset(error, '\0', FUGHCB_MAX_MSG_SIZE* sizeof(char));

    if(FUGHCB40200000_write(serial_port,  ">DON? \r\n", error_msg)){sprintf(error,"%s",error_msg);return FUGHCB_FALIURE;}//DON queries status of the BON

    if(FUGHCB40200000_read(serial_port, reply, error_msg)){sprintf(error,"%s",error_msg);return FUGHCB_FALIURE;}

	if(reply[0] == 'D' && reply[1] == 'O' && reply[2] == 'N')
	{
		if(reply[4] != '1')
			{*status = 0;}
    	else{*status = 1;}
	}
	else{*status = 0; sprintf(error,"READ ERROR");return FUGHCB_FALIURE;}

    return FUGHCB_SUCCESS;
}

int FUGHCB40200000_set_current_res(const int serial_port, int arg, char* error)
{
	char msg[FUGHCB_MAX_MSG_SIZE];
	char reply[FUGHCB_MAX_MSG_SIZE];
	char error_msg[FUGHCB_MAX_MSG_SIZE];
	memset(error,'\0', FUGHCB_MAX_MSG_SIZE);
	sprintf(msg, ">M1I %i \r\n", arg);//sets the resolution and integration time of the current read out. Arg can be set 0-7 for required number of bits, see FUG manual. Default is 5 (18 bit +sign, integration time 80ms).

    if(FUGHCB40200000_write(serial_port, msg, error_msg)){sprintf(error,"%s",error_msg);return FUGHCB_FALIURE;}//status = 0 is off

    if(FUGHCB40200000_read(serial_port, reply, error_msg)){sprintf(error,"%s",error_msg);return FUGHCB_FALIURE;}

    return FUGHCB_SUCCESS;
}

int FUGHCB40200000_set_voltage_res(const int serial_port, int arg, char* error)
{
	char msg[FUGHCB_MAX_MSG_SIZE];
	char reply[FUGHCB_MAX_MSG_SIZE];
	char error_msg[FUGHCB_MAX_MSG_SIZE];
	memset(error,'\0', FUGHCB_MAX_MSG_SIZE);
	sprintf(msg, ">M0I %i \r\n", arg);//sets resolution of the voltage read out

    if(FUGHCB40200000_write(serial_port, msg, error_msg)){sprintf(error,"%s",error_msg);return FUGHCB_FALIURE;}

    if(FUGHCB40200000_read(serial_port, reply, error_msg)){sprintf(error,"%s",error_msg);return FUGHCB_FALIURE;}

    return FUGHCB_SUCCESS;
}

int FUGHCB40200000_set_mode(const int serial_port, int mode_set, char* error)
{
	mode_status = mode_set;//passes a static variable to tell other functions what the mode is.
	char msg[FUGHCB_MAX_MSG_SIZE];
	char reply[FUGHCB_MAX_MSG_SIZE];
	char error_msg[FUGHCB_MAX_MSG_SIZE];
	memset(error,'\0', FUGHCB_MAX_MSG_SIZE);
	sprintf(msg, ">BX %i \r\n", mode_set);//sets the mode of the current, 0 = 200uA, 1 = 20uA. Default, reset function will set this to 0.

    if(FUGHCB40200000_write(serial_port, msg, error_msg)){sprintf(error,"%s",error_msg);return FUGHCB_FALIURE;}

    if(FUGHCB40200000_read(serial_port, reply, error_msg)){sprintf(error,"%s",error_msg);return FUGHCB_FALIURE;}

    return FUGHCB_SUCCESS;
}

int FUGHCB40200000_set_current(const int serial_port, double current, char* error)
{
	char msg[FUGHCB_MAX_MSG_SIZE];
	char reply[FUGHCB_MAX_MSG_SIZE];//sets the current input by the user. The user can put in any value for the given current range, either +/-200 uA or +/- 20 uA. The code will know what the mode is so changes the output data type, -10 -> +10 to the correct form.
    char error_msg[FUGHCB_MAX_MSG_SIZE];
    memset(error,'\0', FUGHCB_MAX_MSG_SIZE);

	if(mode_status != 0){
		sprintf(msg, ">S1 %f \r\n", fabs(current/2));
	}
	else{
		sprintf(msg, ">S1 %f \r\n", fabs(current/20));
	}

    if(FUGHCB40200000_write(serial_port, msg, error_msg)){sprintf(error,"%s",error_msg);return FUGHCB_FALIURE;}

    if(FUGHCB40200000_read(serial_port, reply, error_msg)){sprintf(error,"%s",error_msg);return FUGHCB_FALIURE;}

    return FUGHCB_SUCCESS;
}

int FUGHCB40200000_set_voltage(const int serial_port, int voltage, char* error)
{
	char msg[FUGHCB_MAX_MSG_SIZE];
	char reply[FUGHCB_MAX_MSG_SIZE];
	char error_msg[FUGHCB_MAX_MSG_SIZE];
	memset(error,'\0', FUGHCB_MAX_MSG_SIZE);
	sprintf(msg, ">S0 %i \r\n", voltage); //the data type here is 1 = 1 V.

    if(FUGHCB40200000_write(serial_port, msg, error_msg)){sprintf(error,"%s",error_msg);return FUGHCB_FALIURE;}

    if(FUGHCB40200000_read(serial_port, reply, error_msg)){sprintf(error,"%s",error_msg);return FUGHCB_FALIURE;}

    return FUGHCB_SUCCESS;
}

int FUGHCB40200000_set_ramp_mode(const int serial_port, int mode, char* error)
{
	char msg[FUGHCB_MAX_MSG_SIZE];
	char reply[FUGHCB_MAX_MSG_SIZE];
	char error_msg[FUGHCB_MAX_MSG_SIZE];
	memset(error,'\0', FUGHCB_MAX_MSG_SIZE);
	sprintf(msg, ">S0B %i \r\n", mode);//ramp function comes before the set ramp rate. This defines the mode type, details given in manual. Default for our needs is to set this to 2.

    if(FUGHCB40200000_write(serial_port, msg, error_msg)){sprintf(error,"%s",error_msg);return FUGHCB_FALIURE;}

    if(FUGHCB40200000_read(serial_port, reply, error_msg)){sprintf(error,"%s",error_msg);return FUGHCB_FALIURE;}

    return FUGHCB_SUCCESS;
}


int FUGHCB40200000_set_ramp(const int serial_port, unsigned int voltage, char* error)
{
	char msg[FUGHCB_MAX_MSG_SIZE];
	char reply[FUGHCB_MAX_MSG_SIZE];
	char error_msg[FUGHCB_MAX_MSG_SIZE];
	memset(error,'\0', FUGHCB_MAX_MSG_SIZE);
	sprintf(msg, ">S0R %i \r\n", voltage); //defines the rate, i.e. the number of V/sec. This cannot be larger then 1 kV/sec.

    if(FUGHCB40200000_write(serial_port, msg, error_msg)){sprintf(error,"%s",error_msg);return FUGHCB_FALIURE;}

    if(FUGHCB40200000_read(serial_port, reply, error_msg)){sprintf(error,"%s",error_msg);return FUGHCB_FALIURE;}

    return FUGHCB_SUCCESS;
}

int FUGHCB40200000_measure_current(const int serial_port, float* current, char* error)
{
	char error_msg[FUGHCB_MAX_MSG_SIZE];
	char reply[FUGHCB_MAX_MSG_SIZE];
	memset(error, '\0', FUGHCB_MAX_MSG_SIZE);

    if(FUGHCB40200000_write(serial_port, ">M1? \r\n", error_msg)){sprintf(error,"%s",error_msg);return FUGHCB_FALIURE;}//reads the monitor current, this is constantly updated value. Returns the actual current for given mode.

    if(FUGHCB40200000_read(serial_port, reply, error_msg)){sprintf(error,"%s",error_msg);return FUGHCB_FALIURE;}
	
	if(reply[0] == 'M' && reply[1] == '1')
	{
		sscanf(reply, "%*c%*c%*c%f\n", current);
		if(mode_status !=0){
			*current *= 2;
		}
		else{*current *= 20;}
	}
	else{*current = 0; sprintf(error,"READ ERROR");return FUGHCB_FALIURE;}
	return FUGHCB_SUCCESS;
}

int FUGHCB40200000_measure_voltage(const int serial_port, float* voltage, char* error)
{
    char error_msg[FUGHCB_MAX_MSG_SIZE];
    char reply[FUGHCB_MAX_MSG_SIZE];
    memset(error, '\0', FUGHCB_MAX_MSG_SIZE);

    if(FUGHCB40200000_write(serial_port, ">M0? \r\n", error_msg)){sprintf(error,"%s",error_msg);return FUGHCB_FALIURE;}//reads the monitor current, this is constantly updated value. Returns the actual current for given mode.

    if(FUGHCB40200000_read(serial_port, reply, error_msg)){sprintf(error,"%s",error_msg);return FUGHCB_FALIURE;}


	if(reply[0] == 'M' && reply[1] == '0')
	{
		sscanf(reply, "%*c%*c%*c%f\n", voltage);
	}
	else{*voltage = 0; sprintf(error,"READ ERROR");return FUGHCB_FALIURE;}

	return FUGHCB_SUCCESS;
}

int FUGHCB40200000_get_voltage(const int serial_port, float* voltage, char* error)
{
    char error_msg[FUGHCB_MAX_MSG_SIZE];
    char reply[FUGHCB_MAX_MSG_SIZE];
    memset(error, '\0', FUGHCB_MAX_MSG_SIZE);

    if(FUGHCB40200000_write(serial_port,  ">S0? \r\n", error_msg)){sprintf(error,"%s",error_msg);return FUGHCB_FALIURE;}//reads the monitor current, this is constantly updated value. Returns the actual current for given mode.

    if(FUGHCB40200000_read(serial_port, reply, error_msg)){sprintf(error,"%s",error_msg);return FUGHCB_FALIURE;}

	if(reply[0] == 'S' && reply[1] == '0')
	{
		sscanf(reply, "%*c%*c%*c%f\n", voltage);
	}
	else{*voltage = 0; sprintf(error,"READ ERROR");return FUGHCB_FALIURE;}
	
	return FUGHCB_SUCCESS;
}

int FUGHCB40200000_get_current(const int serial_port, float* current, char* error)
{
    char error_msg[FUGHCB_MAX_MSG_SIZE];
    char reply[FUGHCB_MAX_MSG_SIZE];
    memset(error, '\0', FUGHCB_MAX_MSG_SIZE);

    if(FUGHCB40200000_write(serial_port,  ">S1? \r\n", error_msg)){sprintf(error,"%s",error_msg);return FUGHCB_FALIURE;}//reads the monitor current, this is constantly updated value. Returns the actual current for given mode.

    if(FUGHCB40200000_read(serial_port, reply, error_msg)){sprintf(error,"%s",error_msg);return FUGHCB_FALIURE;}

	if(reply[0] == 'S' && reply[1] == '1')
	{
		sscanf(reply, "%*c%*c%*c%f\n", current);
		if(mode_status !=0){
			*current *= 2;
		}
		else{*current *= 20;}
	}
	else{*current = 0; sprintf(error,"READ ERROR");return FUGHCB_FALIURE;}
	return FUGHCB_SUCCESS;
}

int FUGHCB40200000_get_flashover(const int serial_port, int* status, char* error)
{
    char reply[FUGHCB_MAX_MSG_SIZE];
    char error_msg[FUGHCB_MAX_MSG_SIZE];
    memset(error, '\0', FUGHCB_MAX_MSG_SIZE* sizeof(char));
    memset(status, '\0', FUGHCB_MAX_MSG_SIZE* sizeof(char));

    if(FUGHCB40200000_write(serial_port,  ">D3R? \r\n", error_msg)){sprintf(error,"%s",error_msg);return FUGHCB_FALIURE;}

    if(FUGHCB40200000_read(serial_port, reply, error_msg)){sprintf(error,"%s",error_msg);return FUGHCB_FALIURE;}

	if(reply[0] == 'D' && reply[1] == '3' && reply[2] == 'R')
	{
	    if(reply[4] != '1'){*status = 0;}
    	else{*status = 1;}
	}
	else{*status = 0; sprintf(error,"READ ERROR");return FUGHCB_FALIURE;}

    return FUGHCB_SUCCESS;
}

int FUGHCB40200000_get_cal_lock(const int serial_port, char* state, char* error)
{
    char reply[FUGHCB_MAX_MSG_SIZE];
    char error_msg[FUGHCB_MAX_MSG_SIZE];
    memset(error, '\0', FUGHCB_MAX_MSG_SIZE* sizeof(char));

    if(FUGHCB40200000_write(serial_port,  ">DCAL? \r\n", error_msg)){sprintf(error,"%s",error_msg);return FUGHCB_FALIURE;}

    if(FUGHCB40200000_read(serial_port, state, error_msg)){sprintf(error,"%s",error_msg);return FUGHCB_FALIURE;}

    return FUGHCB_SUCCESS;
}

int FUGHCB40200000_get_ramp_rate(const int serial_port, char* reply, char* error)
{
    char error_msg[FUGHCB_MAX_MSG_SIZE];
    memset(error, '\0', FUGHCB_MAX_MSG_SIZE);

    if(FUGHCB40200000_write(serial_port,  ">S1R? \r\n", error_msg)){sprintf(error,"%s",error_msg);return FUGHCB_FALIURE;}

    if(FUGHCB40200000_read(serial_port, reply, error_msg)){sprintf(error,"%s",error_msg);return FUGHCB_FALIURE;}

    return FUGHCB_SUCCESS;
}

int FUGHCB40200000_get_ramp_mode(const int serial_port, char* reply, char* error)
{
    char error_msg[FUGHCB_MAX_MSG_SIZE];
    memset(error, '\0', FUGHCB_MAX_MSG_SIZE);

    if(FUGHCB40200000_write(serial_port,  ">S1S? \r\n", error_msg)){sprintf(error,"%s",error_msg);return FUGHCB_FALIURE;}

    if(FUGHCB40200000_read(serial_port, reply, error_msg)){sprintf(error,"%s",error_msg);return FUGHCB_FALIURE;}

    return FUGHCB_SUCCESS;
}

