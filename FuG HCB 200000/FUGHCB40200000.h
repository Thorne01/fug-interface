#ifndef __FUGHCB40200000__
#define __FUGHCB40200000__

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <fcntl.h>  
#include <termios.h> 
#include <unistd.h>
#include <errno.h>
#include <sys/ioctl.h>
#include <sys/stat.h>
#include "math.h"
#include <time.h>

#include <sys/uio.h>
#include <sys/socket.h>
#include <sys/select.h>
#define FUGHCB_MAX_MSG_SIZE 512
#define FUGHCB_SUCCESS 0
#define FUGHCB_FALIURE 1
#define FUGHCB_CONNECT_FAIL -1

fd_set readfds_FUGHCB;
struct timeval timeout_FUGHCB;

int FUGHCB40200000_connect(const char* port_id, char* error);

int FUGHCB40200000_configure(const int serial_port, char* error);

int FUGHCB40200000_write(const int serial_port, char* msg, char* error);

int FUGHCB40200000_read(const int serial_port, char* reply, char* error);

int FUGHCB40200000_disconnect(const int);

int FUGHCB40200000_set_reset(int serial_port, char* error);

int FUGHCB40200000_get_identify(const int serial_port, char* reply, char* error);

int FUGHCB40200000_get_SELD(const int serial_port, char* status, char* error);

int FUGHCB40200000_set_interlock(const int serial_port, int status, char* error);

int FUGHCB40200000_set_HV_output(const int serial_port, int status, char* error);

int FUGHCB40200000_get_output_state(const int serial_port, int* status, char* error);

int FUGHCB40200000_set_current_res(const int serial_port, int arg, char* error);

int FUGHCB40200000_set_voltage_res(const int serial_port, int arg, char* error);

int FUGHCB40200000_set_mode(const int serial_port, int mode_set, char* error);

int FUGHCB40200000_set_current(const int serial_port, double current, char* error);

int FUGHCB40200000_set_voltage(const int serial_port, int voltage, char* error);

int FUGHCB40200000_set_ramp_mode(const int serial_port, int mode, char* error);

int FUGHCB40200000_set_ramp(const int serial_port, unsigned int voltage, char* error);

int FUGHCB40200000_measure_current(const int serial_port, float* current, char* error);

int FUGHCB40200000_measure_voltage(const int serial_port, float* voltage, char* error);

int FUGHCB40200000_get_voltage(const int serial_port, float* voltage, char* error);

int FUGHCB40200000_get_current(const int serial_port, float* current, char* error);

int FUGHCB40200000_get_flashover(const int serial_port, int* status, char* error);

int FUGHCB40200000_get_cal_lock(const int serial_port, char* state, char* error);

int FUGHCB40200000_get_ramp_rate(const int serial_port, char* reply, char* error);

int FUGHCB40200000_get_ramp_mode(const int serial_port, char* reply, char* error);

#endif
