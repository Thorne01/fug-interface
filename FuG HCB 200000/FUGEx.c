#include <stdio.h>
#include <string.h>

#include <unistd.h>  /* UNIX Standard Definitions      */

#include "FUGHCB40200000.h"

void flush_stdin(void);
int enter_press();

int main(void)
{
    char FUGHCB_err[FUGHCB_MAX_MSG_SIZE];
    char id[FUGHCB_MAX_MSG_SIZE];
    char SELD_status[FUGHCB_MAX_MSG_SIZE];
    int output_state;

	//dev/FUGHCB_01
	
	int fd = FUGHCB40200000_connect("/dev/serial/by-id/usb-MOXA_Technologies_MOXA_UPort_1610-8-if00-port3", FUGHCB_err);

	if(fd == -1){printf("%s\n",FUGHCB_err); return -1;}

	if(FUGHCB40200000_get_identify(fd, id, FUGHCB_err)){printf("%s\n",FUGHCB_err);return -1;}
	else{printf("Device ID: %s\n",id);}


    /*FILE *fptr;

    //fptr = fopen("/home/jacobthorne/Documents/Example_test.txt","a+");

    //if(fptr == NULL)
    //{
    //    printf("Error opening file!");
    //    exit(1);
    //}

    if(fptr == NULL)
    {
        printf("Error opening file!");
        exit(1);
    }*/


	if(FUGHCB40200000_get_SELD(fd, SELD_status, FUGHCB_err)){printf("%s\n",FUGHCB_err);return -1;}
	else{printf("%s\n", SELD_status);}
	
	if(FUGHCB40200000_set_reset(fd,FUGHCB_err)){printf("%s\n",FUGHCB_err);return -1;}

	if(FUGHCB40200000_set_interlock(fd, 1,FUGHCB_err)){printf("%s\n",FUGHCB_err);return -1;}

	sleep(5);

	if(FUGHCB40200000_set_HV_output(fd, 1,FUGHCB_err)){printf("%s\n",FUGHCB_err);return -1;}

	if(FUGHCB40200000_get_output_state(fd, &output_state, FUGHCB_err)){printf("%s\n",FUGHCB_err);return -1;}
	printf("Device output state: %d\n", output_state);
    if(output_state != 1){printf("FUGHCB: CANNOT ENGAGE OUTPUT!");return -1;}//error handle to check if interlock is open or not

	if(FUGHCB40200000_set_current_res(fd, 5,FUGHCB_err)){printf("%s\n",FUGHCB_err);return -1;} //int 0-7, zero is 14 bit+sign, typ. 5 18 bit +sign

	if(FUGHCB40200000_set_voltage_res(fd, 5,FUGHCB_err)){printf("%s\n",FUGHCB_err);return -1;}

	char cal_state[FUGHCB_MAX_MSG_SIZE];
	if(FUGHCB40200000_get_cal_lock(fd, cal_state, FUGHCB_err)){printf("%s\n",FUGHCB_err);return -1;}
	printf("%s\n",cal_state);

	if(FUGHCB40200000_set_mode(fd, 1,FUGHCB_err)){printf("%s\n",FUGHCB_err);return -1;} //0 = 200 uA range, 1 = 20 uA range

	if(FUGHCB40200000_set_current(fd, 5,FUGHCB_err)){printf("%s\n",FUGHCB_err);return -1;} //current limit, absolute value

	if(FUGHCB40200000_set_ramp_mode(fd, 2,FUGHCB_err)){printf("%s\n",FUGHCB_err);return -1;} //mode; 0 = standard

	char ramp_mode[FUGHCB_MAX_MSG_SIZE];
	if(FUGHCB40200000_get_ramp_mode(fd, ramp_mode, FUGHCB_err)){printf("%s\n",FUGHCB_err);return -1;}
	printf("%s\n",ramp_mode);

	if(FUGHCB40200000_set_ramp(fd, 5000,FUGHCB_err)){printf("%s\n",FUGHCB_err);return -1;} //volts per second is the varible, max is 1kV/sec

	char ramp_rate[FUGHCB_MAX_MSG_SIZE];
	if(FUGHCB40200000_get_ramp_rate(fd, ramp_rate, FUGHCB_err)){printf("%s\n",FUGHCB_err);return -1;}
	printf("%s\n",ramp_rate);

	if(FUGHCB40200000_set_voltage(fd, 1000,FUGHCB_err)){printf("%s\n",FUGHCB_err);return -1;} //voltage in V not kV! This sets the limit to ramp up to

	float curr_lim;
	if(FUGHCB40200000_get_current(fd, &curr_lim,FUGHCB_err)){printf("%s\n",FUGHCB_err);return -1;}
	else{
	printf("Current limit:%f (uA)\n",curr_lim);}

	float volt_lim;
	if(FUGHCB40200000_get_voltage(fd, &volt_lim,FUGHCB_err)){printf("%s\n",FUGHCB_err);return -1;}
	else{
	printf("Voltage limit:%f (V)\n",volt_lim);}

	float curr;
	float volt;
    int i = 0;
    printf("\nMeasurement Step\tVoltage (V)\tCurrent (uA)\n");

    //fprintf(fptr, "\nMeasurement Step\tVoltage (V)\tCurrent (uA)\n");
	while(!enter_press())
	{
		if(FUGHCB40200000_measure_current(fd, &curr,FUGHCB_err)){printf("%s\n",FUGHCB_err);return -1;}

		if(FUGHCB40200000_measure_voltage(fd, &volt,FUGHCB_err)){printf("%s\n",FUGHCB_err);return -1;}

		printf("%d\t%f\t%f\n",i, volt, curr);
        //fprintf(fptr,"%d\t%f\t%f\n",i, volt, curr);
		i++;
	}
	int flash;
	if(FUGHCB40200000_get_flashover(fd, &flash,FUGHCB_err)){printf("%s\n",FUGHCB_err);return -1;}
	
	if(FUGHCB40200000_set_interlock(fd, 0,FUGHCB_err)){printf("%s\n",FUGHCB_err);return -1;}

	if(FUGHCB40200000_set_HV_output(fd, 0,FUGHCB_err)){printf("%s\n",FUGHCB_err);return -1;}

	//fclose(fptr );

	FUGHCB40200000_disconnect(fd);
}

void flush_stdin(void)
{
    int c;
    do {
        c = getchar();
    } while (c != '\n' && c != EOF);
}

int enter_press()
{
    int stdin_value = 0;
    struct timeval tv;
    fd_set fds;
    tv.tv_sec = 0;
    tv.tv_usec = 0;
    FD_ZERO(&fds);
    FD_SET(STDIN_FILENO, &fds); //STDIN_FILENO is 0
    select(STDIN_FILENO+1, &fds, NULL, NULL, &tv);
    stdin_value = FD_ISSET(STDIN_FILENO, &fds);
    if (stdin_value != 0)
        flush_stdin();
    return stdin_value;
}
