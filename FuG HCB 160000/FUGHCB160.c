/******************************************************************************
* @author J.Thorne
*********************************************************************************/

#include "FUGHCB160.h"

static int mode_status;

int FUGHCB160_error_handle(int error_code, char* err_msg)
{
	char error_prefix[] = "FUGHCB160: "; //define prefix of the return for the error
	switch(error_code)
	{
		case 0 :
			break;
		case 1 :
			sprintf(err_msg, "%sNO DATA AVAILABLE\n",error_prefix);
			break;
		case 2 :
			sprintf(err_msg, "%sUNKNOWN REGISTER TYPE\n",error_prefix);
			break;
		case 4 :
			sprintf(err_msg, "%sINVALID ARGUMENT\n",error_prefix);
			break;
		case 5 :
			sprintf(err_msg, "%sARGUMENT OUT OF RANGE\n",error_prefix);
			break;
		case 6 :
			sprintf(err_msg, "%sREGISTER IS READ-ONLY\n",error_prefix);
			break;
		case 7 :
			sprintf(err_msg, "%sRECEIVE OVERFLOW\n",error_prefix);
			break;
		case 8 :
			sprintf(err_msg, "%sEEPROM IS WRITE PROTECTED\n",error_prefix);
			break;
		case 9 :
			sprintf(err_msg, "%sADDRESS ERROR\n",error_prefix);
			break;
		case 10 :
			sprintf(err_msg, "%sUNKNOWN COMMAND\n",error_prefix);
			break;
		case 11 :
			sprintf(err_msg, "%sNOT ALLOWED TRIGGER-ON-TALK\n",error_prefix);
			break;
		case 12 :
			sprintf(err_msg, "%sINVALID ARGUMENT IN ~Tn COMMAND\n",error_prefix);
			break;
		case 13 :
			sprintf(err_msg, "%sINVALID N-VALUE\n",error_prefix);
			break;
		case 14 :
			sprintf(err_msg, "%sREGISTER IS WRITE ONLY\n",error_prefix);
			break;
		case 15 :
			sprintf(err_msg, "%sSTRING TOO LONG\n",error_prefix);
			break;
		case 16 :
			sprintf(err_msg, "%sWRONG CHECKSUM\n",error_prefix);
			break;
		case 17 :
			sprintf(err_msg, "%sINTERLOCK OPEN?\n",error_prefix);
			break;
		case 18 :
			sprintf(err_msg, "%sDIGITAL PROGRAMMING NOT ACTIVE\n",error_prefix);
			break;
		case 19 :
			sprintf(err_msg, "%sERROR WRITING TO THE DEVICE\n",error_prefix);
			break;
		default :
			sprintf(err_msg, "%sUNKNOWN ERROR CODE\n",error_prefix);
	}
	return FUGHCB160_SUCCESS;
} 
//list of codes and meanings

int FUGHCB160_connect(const char* port_id, char* error)
{
    memset(error, '\0', FUGHCB160_MSG_SIZE* sizeof(char));
	int serial_port = open(port_id, O_RDWR | O_NOCTTY | O_SYNC);
	if(serial_port == -1)                        // Error Checking 
	{
		sprintf(error, "FUGHCB160: ERROR OPENING DEVICE");
		return FUGHCB160_CONNECT_FAIL;
	}
	else
	{
        if(FUGHCB160_configure(serial_port, error))
        {
            tcflush(serial_port,TCIOFLUSH);
            return FUGHCB160_CONNECT_FAIL;
        }
	}
	return serial_port;
}

int FUGHCB160_configure(const int serial_port, char* error){
		   	
	struct termios options;
	memset(&options, 0, sizeof(options));  /* clear the new struct */
    memset(error, 0, FUGHCB160_MSG_SIZE*sizeof(char));

	cfsetispeed(&options, B9600); //baud rate
	cfsetospeed(&options, B9600);
	options.c_iflag &= ~(IGNBRK | BRKINT | ICRNL | INLCR | PARMRK | INPCK | ISTRIP | IXON);
	options.c_oflag = 0;
	options.c_lflag &= ~(ECHO | ECHONL | IEXTEN );
	options.c_lflag |= ICANON | ISIG;
	options.c_cflag &= ~(CSIZE | PARENB);
	options.c_cflag |= CS8;
   	options.c_cflag |= CLOCAL | CREAD;

	options.c_cc[VMIN]  = 0; 
 	options.c_cc[VTIME] = 0;
	options.c_cc[VEOL] = 0;
	options.c_cc[VEOL2] = 0;
	options.c_cc[VEOF] = 0x04;

	
	if(tcsetattr(serial_port, TCSANOW, &options) != 0)
  	{
		sprintf(error, "FUGHCB160: FAILURE TO CONFIGURE TERMIOS");
		return FUGHCB160_FALIURE;
  	}
	else
	{
		return FUGHCB160_SUCCESS;
	}
}

int FUGHCB160_write(const int serial_port, char* msg, char* error)
{
	unsigned int status = 0x00;
	ioctl(serial_port, TIOCMGET, &status);//define type of message
    memset(error, '\0', FUGHCB160_MSG_SIZE);
	int nbytes_written = write(serial_port, msg, strlen(msg)+1);//builds the message to be sent
	
	if(nbytes_written != strlen(msg)+1)
		{FUGHCB160_error_handle(19, error); sprintf(error, "write errno error: %d",errno);
		return FUGHCB160_FALIURE;} //error check
	else
		{return FUGHCB160_SUCCESS;}
}

int FUGHCB160_read(const int serial_port, char* reply, char* error)
{
	int retval = 0;
	memset(reply, '\0', FUGHCB160_MSG_SIZE* sizeof(char));

	FD_ZERO(&readfds_FUGHCB160);
	FD_SET(serial_port, &readfds_FUGHCB160);

	timeout_FUGHCB160.tv_sec = 1; //1 second timeout
	timeout_FUGHCB160.tv_usec = 0; 

	retval = select(serial_port+1, &readfds_FUGHCB160, NULL, NULL, &timeout_FUGHCB160);

	if(retval == -1){
		sprintf(error, "FUGHCB160 select() errno: %d", errno);
	}
	else if(retval == 0)
	{
		sprintf(error, "FUGHCB160: READ TIMEOUT!");
        return FUGHCB160_FALIURE;
	}

    read(serial_port, reply, FUGHCB160_MSG_SIZE);

	if(reply[0] == 'E' && reply[1] != '0')//if message is accepted, will return E0. However, some queries can reply with M1:## so this is taken into account. Any error from the PS will return E1-16. 
	{
		int ret_error_code;
		unsigned int msg_length = strlen(reply);
		char char_error_code[msg_length];
		memcpy(char_error_code, reply+1, msg_length);
		sscanf(char_error_code, "%d", &ret_error_code);
        FUGHCB160_error_handle(ret_error_code, error);
		return FUGHCB160_FALIURE;
	}	
	else
	{return FUGHCB160_SUCCESS;}
}

int FUGHCB160_disconnect(const int serial_port)
{
	return close(serial_port);
}

int FUGHCB160_set_reset(int serial_port, char* error)
{
	char reply[FUGHCB160_MSG_SIZE];
	char error_msg[FUGHCB160_MSG_SIZE];
    memset(error, '\0', FUGHCB160_MSG_SIZE *sizeof(char));

    if(FUGHCB160_write(serial_port, ">B0 1 \r\n", error_msg)){sprintf(error,"%s",error_msg);return FUGHCB160_FALIURE;}

    if(FUGHCB160_read(serial_port, reply, error_msg)){sprintf(error,"%s",error_msg);return FUGHCB160_FALIURE;}

	return FUGHCB160_SUCCESS;
}

int FUGHCB160_get_identify(const int serial_port, char* reply, char* error)
{
    char error_msg[FUGHCB160_MSG_SIZE];
    memset(reply, '\0', FUGHCB160_MSG_SIZE* sizeof(char));
    memset(error, '\0', FUGHCB160_MSG_SIZE* sizeof(char));

    if(FUGHCB160_write(serial_port, "*IDN? \r\n", error_msg)){sprintf(error,"%s",error_msg);return FUGHCB160_FALIURE;}

    if(FUGHCB160_read(serial_port, reply, error_msg)){sprintf(error,"%s",error_msg);return FUGHCB160_FALIURE;}

    return FUGHCB160_SUCCESS;
}

int FUGHCB160_get_SELD(const int serial_port, char* status, char* error)
{
	char reply[FUGHCB160_MSG_SIZE];
    char error_msg[FUGHCB160_MSG_SIZE];
    memset(error, '\0', FUGHCB160_MSG_SIZE* sizeof(char));
    memset(status, '\0', FUGHCB160_MSG_SIZE* sizeof(char));

    if(FUGHCB160_write(serial_port, ">DSD? \r\n", error_msg)){sprintf(error,"%s",error_msg);return FUGHCB160_FALIURE;}

    if(FUGHCB160_read(serial_port, reply, error_msg)){sprintf(error,"%s",error_msg);return FUGHCB160_FALIURE;}

	if(reply[0] == 'D' && reply[1] == 'S' && reply[2] == 'D')
	{
		if(reply[4] != '1'){FUGHCB160_error_handle(18, error);return FUGHCB160_FALIURE;}//will return special error if there is an issue
		else{sprintf(status, "FUGHCB160: Digital control active!");}
	}
	else{sprintf(status, '\0'); sprintf(error,"READ ERROR");return FUGHCB160_FALIURE;}
	return FUGHCB160_SUCCESS;
}

int FUGHCB160_set_HV_output(const int serial_port, int status, char* error)
{
	char msg[FUGHCB160_MSG_SIZE];
	char reply[FUGHCB160_MSG_SIZE];
	char error_msg[FUGHCB160_MSG_SIZE];
	memset(error,'\0', FUGHCB160_MSG_SIZE);
	sprintf(msg, ">BON %i \r\n", status); //BON command has to be set with B2. BON turns the output on and off.

    if(FUGHCB160_write(serial_port, msg, error_msg)){sprintf(error,"%s",error_msg);return FUGHCB160_FALIURE;}//status = 0 is off

    if(FUGHCB160_read(serial_port, reply, error_msg)){sprintf(error,"%s",error_msg);return FUGHCB160_FALIURE;}

    return FUGHCB160_SUCCESS;
}

int FUGHCB160_get_output_state(const int serial_port, int* status, char* error)
{
	char reply[FUGHCB160_MSG_SIZE];
    char error_msg[FUGHCB160_MSG_SIZE];
    memset(error, '\0', FUGHCB160_MSG_SIZE* sizeof(char));

    if(FUGHCB160_write(serial_port,  ">DON? \r\n", error_msg)){sprintf(error,"%s",error_msg);return FUGHCB160_FALIURE;}//DON queries status of the BON

    if(FUGHCB160_read(serial_port, reply, error_msg)){sprintf(error,"%s",error_msg);return FUGHCB160_FALIURE;}

	if(reply[0] == 'D' && reply[1] == 'O' && reply[2] == 'N')
	{
		if(reply[4] != '1')
			{*status = 0;}
    	else{*status = 1;}
	}
	else{*status = 0; sprintf(error,"READ ERROR");return FUGHCB160_FALIURE;}

    return FUGHCB160_SUCCESS;
}

int FUGHCB160_set_voltage(const int serial_port, int voltage, char* error)
{
	char msg[FUGHCB160_MSG_SIZE];
	char reply[FUGHCB160_MSG_SIZE];
	char error_msg[FUGHCB160_MSG_SIZE];
	memset(error,'\0', FUGHCB160_MSG_SIZE);
	sprintf(msg, ">S0 %i \r\n", voltage); //the data type here is 1 = 1 V.

    if(FUGHCB160_write(serial_port, msg, error_msg)){sprintf(error,"%s",error_msg);return FUGHCB160_FALIURE;}

    if(FUGHCB160_read(serial_port, reply, error_msg)){sprintf(error,"%s",error_msg);return FUGHCB160_FALIURE;}

    return FUGHCB160_SUCCESS;
}

int FUGHCB160_measure_current(const int serial_port, float* current, char* error)
{
	char error_msg[FUGHCB160_MSG_SIZE];
	char reply[FUGHCB160_MSG_SIZE];
	memset(error, '\0', FUGHCB160_MSG_SIZE);

    if(FUGHCB160_write(serial_port, ">M1? \r\n", error_msg)){sprintf(error,"%s",error_msg);return FUGHCB160_FALIURE;}//reads the monitor current, this is constantly updated value. Returns the actual current for given mode.

    if(FUGHCB160_read(serial_port, reply, error_msg)){sprintf(error,"%s",error_msg);return FUGHCB160_FALIURE;}
	
	if(reply[0] == 'M' && reply[1] == '1')
	{
		sscanf(reply, "%*c%*c%*c%f\n", current);
	}
	else{*current = 0; sprintf(error,"READ ERROR");return FUGHCB160_FALIURE;}
	return FUGHCB160_SUCCESS;
}

int FUGHCB160_measure_voltage(const int serial_port, float* voltage, char* error)
{
    char error_msg[FUGHCB160_MSG_SIZE];
    char reply[FUGHCB160_MSG_SIZE];
    memset(error, '\0', FUGHCB160_MSG_SIZE);

    if(FUGHCB160_write(serial_port, ">M0? \r\n", error_msg)){sprintf(error,"%s",error_msg);return FUGHCB160_FALIURE;}//reads the monitor current, this is constantly updated value. Returns the actual current for given mode.

    if(FUGHCB160_read(serial_port, reply, error_msg)){sprintf(error,"%s",error_msg);return FUGHCB160_FALIURE;}


	if(reply[0] == 'M' && reply[1] == '0')
	{
		sscanf(reply, "%*c%*c%*c%f\n", voltage);
	}
	else{*voltage = 0; sprintf(error,"READ ERROR");return FUGHCB160_FALIURE;}

	return FUGHCB160_SUCCESS;
}

int FUGHCB160_get_voltage(const int serial_port, float* voltage, char* error)
{
    char error_msg[FUGHCB160_MSG_SIZE];
    char reply[FUGHCB160_MSG_SIZE];
    memset(error, '\0', FUGHCB160_MSG_SIZE);

    if(FUGHCB160_write(serial_port,  ">S0? \r\n", error_msg)){sprintf(error,"%s",error_msg);return FUGHCB160_FALIURE;}//reads the monitor current, this is constantly updated value. Returns the actual current for given mode.

    if(FUGHCB160_read(serial_port, reply, error_msg)){sprintf(error,"%s",error_msg);return FUGHCB160_FALIURE;}

	if(reply[0] == 'S' && reply[1] == '0')
	{
		sscanf(reply, "%*c%*c%*c%f\n", voltage);
	}
	else{*voltage = 0; sprintf(error,"READ ERROR");return FUGHCB160_FALIURE;}
	
	return FUGHCB160_SUCCESS;
}
