#ifndef __FUGHCB160__
#define __FUGHCB160__

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <fcntl.h>  
#include <termios.h> 
#include <unistd.h>
#include <errno.h>
#include <sys/ioctl.h>
#include <sys/stat.h>
#include "math.h"
#include <time.h>

#include <sys/uio.h>
#include <sys/socket.h>
#include <sys/select.h>
#define FUGHCB160_MSG_SIZE 512
#define FUGHCB160_SUCCESS 0
#define FUGHCB160_FALIURE 1
#define FUGHCB160_CONNECT_FAIL -1

fd_set readfds_FUGHCB160;
struct timeval timeout_FUGHCB160;

int FUGHCB160_connect(const char* port_id, char* error);

int FUGHCB160_configure(const int serial_port, char* error);

int FUGHCB160_write(const int serial_port, char* msg, char* error);

int FUGHCB160_read(const int serial_port, char* reply, char* error);

int FUGHCB160_disconnect(const int);

int FUGHCB160_set_reset(int serial_port, char* error);

int FUGHCB160_get_identify(const int serial_port, char* reply, char* error);

int FUGHCB160_get_SELD(const int serial_port, char* status, char* error);

int FUGHCB160_set_HV_output(const int serial_port, int status, char* error);

int FUGHCB160_get_output_state(const int serial_port, int* status, char* error);

int FUGHCB160_set_voltage(const int serial_port, int voltage, char* error);

int FUGHCB160_measure_current(const int serial_port, float* current, char* error);

int FUGHCB160_measure_voltage(const int serial_port, float* voltage, char* error);

int FUGHCB160_get_voltage(const int serial_port, float* voltage, char* error);

#endif
