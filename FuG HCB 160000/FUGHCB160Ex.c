#include <stdio.h>
#include <string.h>

#include <unistd.h>  /* UNIX Standard Definitions      */

#include "FUGHCB160.h"

void flush_stdin(void);
int enter_press();

int main(void)
{
    char FUGHCB160_err[FUGHCB160_MSG_SIZE];
    char id[FUGHCB160_MSG_SIZE];
    char SELD_status[FUGHCB160_MSG_SIZE];
    int output_state;

	//dev/FUGHCB_01
	
	int fd = FUGHCB160_connect("/dev/serial/by-id/usb-MOXA_Technologies_MOXA_UPort_1610-8-if00-port4", FUGHCB160_err);

	if(fd == -1){printf("%s\n",FUGHCB160_err); return -1;}

	if(FUGHCB160_get_identify(fd, id, FUGHCB160_err)){printf("%s\n",FUGHCB160_err);return -1;}
	else{printf("Device ID: %s\n",id);}

	if(FUGHCB160_get_SELD(fd, SELD_status, FUGHCB160_err)){printf("%s\n",FUGHCB160_err);return -1;}
	else{printf("%s\n", SELD_status);}
	
	if(FUGHCB160_set_reset(fd,FUGHCB160_err)){printf("%s\n",FUGHCB160_err);return -1;}

	if(FUGHCB160_set_HV_output(fd, 1,FUGHCB160_err)){printf("%s\n",FUGHCB160_err);return -1;}

	if(FUGHCB160_get_output_state(fd, &output_state, FUGHCB160_err)){printf("%s\n",FUGHCB160_err);return -1;}
	printf("Device output state: %d\n", output_state);
    if(output_state != 1){printf("FUGHCB: CANNOT ENGAGE OUTPUT!\n");return -1;}//error handle to check if interlock is open or not

	if(FUGHCB160_set_voltage(fd, 1000,FUGHCB160_err)){printf("%s\n",FUGHCB160_err);return -1;} //voltage in V not kV! This sets the limit to ramp up to

	float volt_lim;
	if(FUGHCB160_get_voltage(fd, &volt_lim,FUGHCB160_err)){printf("%s\n",FUGHCB160_err);return -1;}
	else{
	printf("Voltage limit:%f (V)\n",volt_lim);}

	float curr;
	float volt;
    int i = 0;
    printf("\nMeasurement Step\tVoltage (V)\tCurrent (uA)\n");

    //fprintf(fptr, "\nMeasurement Step\tVoltage (V)\tCurrent (uA)\n");
	while(!enter_press())
	{
		if(FUGHCB160_measure_current(fd, &curr,FUGHCB160_err)){printf("%s\n",FUGHCB160_err);return -1;}

		if(FUGHCB160_measure_voltage(fd, &volt,FUGHCB160_err)){printf("%s\n",FUGHCB160_err);return -1;}

		printf("%d\t%f\t%f\n",i, volt, curr);
        //fprintf(fptr,"%d\t%f\t%f\n",i, volt, curr);
		i++;
	}

	if(FUGHCB160_set_HV_output(fd, 0,FUGHCB160_err)){printf("%s\n",FUGHCB160_err);return -1;}

	//fclose(fptr );

	FUGHCB160_disconnect(fd);
}

void flush_stdin(void)
{
    int c;
    do {
        c = getchar();
    } while (c != '\n' && c != EOF);
}

int enter_press()
{
    int stdin_value = 0;
    struct timeval tv;
    fd_set fds;
    tv.tv_sec = 0;
    tv.tv_usec = 0;
    FD_ZERO(&fds);
    FD_SET(STDIN_FILENO, &fds); //STDIN_FILENO is 0
    select(STDIN_FILENO+1, &fds, NULL, NULL, &tv);
    stdin_value = FD_ISSET(STDIN_FILENO, &fds);
    if (stdin_value != 0)
        flush_stdin();
    return stdin_value;
}
