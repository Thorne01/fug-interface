CMAKE_MINIMUM_REQUIRED(VERSION 3.0)
project(FUGNTN)

add_library(FUGNTN FUGNTN.c)

add_executable(FUGNTNEx FUGNTNEx.c)
target_link_libraries(FUGNTNEx FUGNTN)

add_executable(FUGNTNOff FUGNTNOff.c)
target_link_libraries(FUGNTNOff FUGNTN)

add_executable(FUGNTNOn FUGNTNOn.c)
target_link_libraries(FUGNTNOn FUGNTN)
