#include <stdio.h>
#include <string.h>

#include "FUGNTN.h"

void flush_stdin(void);
int enter_press();

int main(void)
{
    char FUGNTN_err[FUGNTN_MSG_SIZE];
    char id[FUGNTN_MSG_SIZE];
    int output_state;

    int fd = FUGNTN_connect("/dev/ttyUSB0", FUGNTN_err);
    if(fd == -1){printf("%s\n",FUGNTN_err); return -1;}

    if(FUGNTN_get_identify(fd, id, FUGNTN_err)){printf("%s\n",FUGNTN_err);return -1;}
    else{printf("Device ID: %s\n",id);}

    //FILE *fptr;

    //fptr = fopen("/home/neutron/Example_test.txt","a+");

    //if(fptr == NULL)
    //{
    //    printf("Error opening file!");
    //    exit(1);
    //}

    //if(FUGNTN_get_SELD(fd, FUGNTN_err)){printf("%s\n",FUGNTN_err);return -1;}

    if(FUGNTN_set_reset(fd,FUGNTN_err)){printf("%s\n",FUGNTN_err);return -1;}

    if(FUGNTN_set_HV_output(fd, 1,FUGNTN_err)){printf("%s\n",FUGNTN_err);return -1;}

    if(FUGNTN_get_output_state(fd, &output_state, FUGNTN_err)){printf("%s\n",FUGNTN_err);return -1;}
    printf("Device output state: %d\n", output_state);
    if(output_state != 1){printf("FUGHCB: CANNOT ENGAGE OUTPUT!");return -1;}//error handle to check if interlock is open or not

    //if(FUGNTN_set_current_res(fd, 5,FUGNTN_err)){printf("%s\n",FUGNTN_err);return -1;} //int 0-7, zero is 14 bit+sign, typ. 5 18 bit +sign

    //if(FUGNTN_set_voltage_res(fd, 5,FUGNTN_err)){printf("%s\n",FUGNTN_err);return -1;}

    if(FUGNTN_set_current(fd, 0.00001,FUGNTN_err)){printf("%s\n",FUGNTN_err);return -1;} //current limit, absolute value

    if(FUGNTN_set_voltage(fd, 35000,FUGNTN_err)){printf("%s\n",FUGNTN_err);return -1;} //voltage in V not kV! This sets the limit to ramp up to

    float curr_lim;
    if(FUGNTN_get_current(fd, &curr_lim,FUGNTN_err)){printf("%s\n",FUGNTN_err);return -1;}
    else{
        printf("Current limit:%f (uA)\n",curr_lim);}

    float volt_lim;
    if(FUGNTN_get_voltage(fd, &volt_lim,FUGNTN_err)){printf("%s\n",FUGNTN_err);return -1;}
    else{
        printf("Voltage limit:%f (V)\n",volt_lim);}

    float curr;
    float volt;
    int i = 0;
    printf("\nMeasurement Step\tVoltage (V)\tCurrent (uA)\n");

    //fprintf(fptr, "\nMeasurement Step\tVoltage (V)\tCurrent (uA)\n");
    while(!enter_press())
    {
        if(FUGNTN_measure_current(fd, &curr,FUGNTN_err)){printf("%s\n",FUGNTN_err);return -1;}

        if(FUGNTN_measure_voltage(fd, &volt,FUGNTN_err)){printf("%s\n",FUGNTN_err);return -1;}

        printf("%d\t%f\t%f\n",i, volt, curr);
        //fprintf(fptr,"%d\t%f\t%f\n",i, volt, curr);
        i++;
    }

    if(FUGNTN_set_HV_output(fd, 0,FUGNTN_err)){printf("%s\n",FUGNTN_err);return -1;}

	//fclose( FILE *fptr );

    FUGNTN_disconnect(fd);
}

void flush_stdin(void)
{
    int c;
    do {
        c = getchar();
    } while (c != '\n' && c != EOF);
}

int enter_press()
{
    int stdin_value = 0;
    struct timeval tv;
    fd_set fds;
    tv.tv_sec = 0;
    tv.tv_usec = 0;
    FD_ZERO(&fds);
    FD_SET(STDIN_FILENO, &fds); //STDIN_FILENO is 0
    select(STDIN_FILENO+1, &fds, NULL, NULL, &tv);
    stdin_value = FD_ISSET(STDIN_FILENO, &fds);
    if (stdin_value != 0)
        flush_stdin();
    return stdin_value;
}
