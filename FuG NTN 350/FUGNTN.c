/******************************************************************************
* @author J.Thorne
*********************************************************************************/

#include "FUGNTN.h"

int FUGNTN_error_handle(int error_code, char* err_msg)
{
    char error_prefix[] = "FUGNTN: "; //define prefix of the return for the error
    switch(error_code)
    {
        case 0 :
            break;
        case 1 :
            sprintf(err_msg, "%sNO DATA AVAILABLE",error_prefix);
            break;
        case 2 :
            sprintf(err_msg, "%sUNKNOWN REGISTER TYPE",error_prefix);
            break;
        case 4 :
            sprintf(err_msg, "%sINVALID ARGUMENT",error_prefix);
            break;
        case 5 :
            sprintf(err_msg, "%sARGUMENT OUT OF RANGE",error_prefix);
            break;
        case 6 :
            sprintf(err_msg, "%sREGISTER IS READ-ONLY",error_prefix);
            break;
        case 7 :
            sprintf(err_msg, "%sRECEIVE OVERFLOW",error_prefix);
            break;
        case 8 :
            sprintf(err_msg, "%sEEPROM IS WRITE PROTECTED",error_prefix);
            break;
        case 9 :
            sprintf(err_msg, "%sADDRESS ERROR",error_prefix);
            break;
        case 10 :
            sprintf(err_msg, "%sUNKNOWN COMMAND",error_prefix);
            break;
        case 11 :
            sprintf(err_msg, "%sNOT ALLOWED TRIGGER-ON-TALK",error_prefix);
            break;
        case 12 :
            sprintf(err_msg, "%sINVALID ARGUMENT IN ~Tn COMMAND",error_prefix);
            break;
        case 13 :
            sprintf(err_msg, "%sINVALID N-VALUE",error_prefix);
            break;
        case 14 :
            sprintf(err_msg, "%sREGISTER IS WRITE ONLY",error_prefix);
            break;
        case 15 :
            sprintf(err_msg, "%sSTRING TOO LONG",error_prefix);
            break;
        case 16 :
            sprintf(err_msg, "%sWRONG CHECKSUM",error_prefix);
            break;
        case 17 :
            sprintf(err_msg, "%sINTERLOCK OPEN?",error_prefix);
            break;
        case 18 :
            sprintf(err_msg, "%sDIGITAL PROGRAMMING NOT ACTIVE",error_prefix);
            break;
        case 19 :
            sprintf(err_msg, "%sERROR WRITING TO THE DEVICE",error_prefix);
            break;
        default :
            sprintf(err_msg, "%sUNKNOWN ERROR CODE",error_prefix);
    }
    return FUGNTN_SUCCESS;
}
//list of codes and meanings

int FUGNTN_connect(const char* port_id, char* error)
{
    memset(error, '\0', FUGNTN_MSG_SIZE * sizeof(char));
    int serial_port = open(port_id, O_RDWR | O_NOCTTY | O_SYNC);
    if (serial_port == -1)
    {
        sprintf(error, "FUGNTN: ERROR OPENING DEVICE");
        return FUGNTN_CONNECT_FAIL;
    } else {
        if (FUGNTN_configure(serial_port, error)) {
            tcflush(serial_port, TCIOFLUSH);
            return FUGNTN_CONNECT_FAIL;
        }
    }
    return serial_port;
}

int FUGNTN_configure(const int serial_port, char* error){

    struct termios options;
    memset(&options, 0, sizeof(options));  /* clear the new struct */
    memset(error, 0, FUGNTN_MSG_SIZE*sizeof(char));

    cfsetispeed(&options, B115200); //baud rate
    cfsetospeed(&options, B115200);
    options.c_iflag &= ~(IGNBRK | BRKINT | ICRNL | INLCR | PARMRK | INPCK | ISTRIP | IXON);
    options.c_oflag = 0;
    options.c_lflag &= ~(ECHO | ECHONL | IEXTEN );
    options.c_lflag |= ICANON | ISIG;
    options.c_cflag &= ~(CSIZE | PARENB);
    options.c_cflag |= CS8;
    options.c_cflag |= CLOCAL | CREAD;

    options.c_cc[VMIN]  = 0;
    options.c_cc[VTIME] = 0;
    options.c_cc[VEOL] = 0;
    options.c_cc[VEOL2] = 0;
    options.c_cc[VEOF] = 0x04;


    if(tcsetattr(serial_port, TCSANOW, &options) != 0)
    {
        sprintf(error, "FUGNTN: FAILURE TO CONFIGURE TERMIOS");
        return FUGNTN_FAILURE;
    }
    else
    {
        return FUGNTN_SUCCESS;
    }
}

int FUGNTN_write(const int serial_port, char* msg, char* error)
{
    unsigned int status = 0x00;
    ioctl(serial_port, TIOCMGET, &status);//define type of message
    memset(error, '\0', FUGNTN_MSG_SIZE);
    int nbytes_written = write(serial_port, msg, strlen(msg)+1);//builds the message to be sent

    if(nbytes_written != strlen(msg)+1)
    {FUGNTN_error_handle(19, error);
        return FUGNTN_FAILURE;} //error check
    else
    {return FUGNTN_SUCCESS;}
}

int FUGNTN_read(const int serial_port, char* reply, char* error)
{
	int retval = 0;
    struct timeval timeout;
    fd_set readfds;

    memset(reply, '\0', FUGNTN_MSG_SIZE* sizeof(char));

    /*int bytes_available = 0;
    long msec = 0, trigger = 5000; //5s to wait before timeout
    clock_t before = clock();
    while(bytes_available<1)
    {
        clock_t difference = clock() - before;
        msec = difference * 1000 / CLOCKS_PER_SEC; //50-60 msec delay for communication
        ioctl(serial_port, FIONREAD, &bytes_available);
        if(msec > trigger)
        {
            sprintf(error, "FUGNTN: READ TIMEOUT");
            return FUGNTN_FAILURE;
        }
    }*/

	FD_ZERO(&readfds);
	FD_SET(serial_port, &readfds);

	timeout.tv_sec = 1; //1 second timeout
	timeout.tv_usec = 0;

	retval = select(serial_port+1, &readfds, NULL, NULL, &timeout);

	if(retval == -1){
		sprintf(error, "FUGNTN select() errno: %d", errno);
	}
	else if(retval == 0)
	{
		sprintf(error, "FUGNTN: READ TIMEOUT!");
        return FUGNTN_FAILURE;
	}

    read(serial_port, reply, FUGNTN_MSG_SIZE);

    if(reply[0] == 'E' && reply[1] != '0')//if message is accepted, will return E0. However, some queries can reply with M1:## so this is taken into account. Any error from the PS will return E1-16.
    {
        int ret_error_code;
        unsigned int msg_length = strlen(reply);
        char char_error_code[msg_length];
        memcpy(char_error_code, reply+1, msg_length);
        sscanf(char_error_code, "%d", &ret_error_code);
        FUGNTN_error_handle(ret_error_code, error);
        return FUGNTN_FAILURE;
    }
    else
    {return FUGNTN_SUCCESS;}
}

int FUGNTN_disconnect(const int serial_port)
{
	return close(serial_port);
}

int FUGNTN_set_reset(int serial_port, char* error)
{
    char reply[FUGNTN_MSG_SIZE];
    char error_msg[FUGNTN_MSG_SIZE];
    memset(error, '\0', FUGNTN_MSG_SIZE *sizeof(char));

    if(FUGNTN_write(serial_port, "= \r\n", error_msg)){sprintf(error,"%s",error_msg);return FUGNTN_FAILURE;}

    if(FUGNTN_read(serial_port, reply, error_msg)){sprintf(error,"%s",error_msg);return FUGNTN_FAILURE;}

    return FUGNTN_SUCCESS;
}

int FUGNTN_get_identify(const int serial_port, char* reply, char* error)
{
    char error_msg[FUGNTN_MSG_SIZE];
    memset(reply, '\0', FUGNTN_MSG_SIZE* sizeof(char));
    memset(error, '\0', FUGNTN_MSG_SIZE* sizeof(char));

    if(FUGNTN_write(serial_port, "*IDN? \r\n", error_msg)){sprintf(error,"%s",error_msg);return FUGNTN_FAILURE;}

    if(FUGNTN_read(serial_port, reply, error_msg)){sprintf(error,"%s",error_msg);return FUGNTN_FAILURE;}

    return FUGNTN_SUCCESS;
}

int FUGNTN_get_SELD(const int serial_port, char* error)
{
    char reply[FUGNTN_MSG_SIZE];
    char error_msg[FUGNTN_MSG_SIZE];
    memset(error, '\0', FUGNTN_MSG_SIZE* sizeof(char));

    if(FUGNTN_write(serial_port, ">DSD? \r\n", error_msg)){sprintf(error,"%s",error_msg);return FUGNTN_FAILURE;}
    if(FUGNTN_read(serial_port, reply, error_msg)){sprintf(error,"%s",error_msg);return FUGNTN_FAILURE;}

	if(reply[0] == 'D' && reply[1] == 'S' && reply[2] == 'D')
	{
    	if(reply[4] != '1'){FUGNTN_error_handle(18, error);return FUGNTN_FAILURE;}//will return special error if there is an issue
	}
	else{sprintf(error, '\0'); sprintf(error,"READ ERROR");return FUGNTN_FAILURE;}
    if(reply[4] != '1'){FUGNTN_error_handle(18, error);return FUGNTN_FAILURE;}//will return special error if there is an issue
    return FUGNTN_SUCCESS;
}

int FUGNTN_set_HV_output(const int serial_port, int status, char* error)
{
    char msg[FUGNTN_MSG_SIZE];
    char reply[FUGNTN_MSG_SIZE];
    char error_msg[FUGNTN_MSG_SIZE];
    memset(error,'\0', FUGNTN_MSG_SIZE);
    sprintf(msg, ">BON %i \r\n", status); //BON command has to be set with B2. BON turns the output on and off.

    if(FUGNTN_write(serial_port, msg, error_msg)){sprintf(error,"%s",error_msg);return FUGNTN_FAILURE;}//status = 0 is off

    if(FUGNTN_read(serial_port, reply, error_msg)){sprintf(error,"%s",error_msg);return FUGNTN_FAILURE;}

    return FUGNTN_SUCCESS;
}

int FUGNTN_get_output_state(const int serial_port, int* status, char* error)
{
    char reply[FUGNTN_MSG_SIZE];
    char error_msg[FUGNTN_MSG_SIZE];
    memset(error, '\0', FUGNTN_MSG_SIZE* sizeof(char));

    if(FUGNTN_write(serial_port,  ">DON? \r\n", error_msg)){sprintf(error,"%s",error_msg);return FUGNTN_FAILURE;}//DON queries status of the BON

    if(FUGNTN_read(serial_port, reply, error_msg)){sprintf(error,"%s",error_msg);return FUGNTN_FAILURE;}

	if(reply[0] == 'D' && reply[1] == 'O' && reply[2] == 'N')
	{
    	if(reply[4] != '1'){*status = 0;}
    	else{*status = 1;}
	}
	else{*status = 0; sprintf(error,"READ ERROR");return FUGNTN_FAILURE;}

    return FUGNTN_SUCCESS;
}

int FUGNTN_set_current_res(const int serial_port, int arg, char* error)
{
    char msg[FUGNTN_MSG_SIZE];
    char reply[FUGNTN_MSG_SIZE];
    char error_msg[FUGNTN_MSG_SIZE];
    memset(error,'\0', FUGNTN_MSG_SIZE);
    sprintf(msg, ">M1I %i \r\n", arg);//sets the resolution and integration time of the current read out. Arg can be set 0-7 for required number of bits, see FUG manual. Default is 5 (18 bit +sign, integration time 80ms).

    if(FUGNTN_write(serial_port, msg, error_msg)){sprintf(error,"%s",error_msg);return FUGNTN_FAILURE;}//status = 0 is off

    if(FUGNTN_read(serial_port, reply, error_msg)){sprintf(error,"%s",error_msg);return FUGNTN_FAILURE;}

    return FUGNTN_SUCCESS;
}

int FUGNTN_set_voltage_res(const int serial_port, int arg, char* error)
{
    char msg[FUGNTN_MSG_SIZE];
    char reply[FUGNTN_MSG_SIZE];
    char error_msg[FUGNTN_MSG_SIZE];
    memset(error,'\0', FUGNTN_MSG_SIZE);
    sprintf(msg, ">M0I %i \r\n", arg);//sets resolution of the voltage read out

    if(FUGNTN_write(serial_port, msg, error_msg)){sprintf(error,"%s",error_msg);return FUGNTN_FAILURE;}

    if(FUGNTN_read(serial_port, reply, error_msg)){sprintf(error,"%s",error_msg);return FUGNTN_FAILURE;}

    return FUGNTN_SUCCESS;
}

int FUGNTN_set_current(const int serial_port, double current, char* error)
{
    char msg[FUGNTN_MSG_SIZE];
    char reply[FUGNTN_MSG_SIZE];//sets the current input by the user. The user can put in any value for the given current range, either +/-200 uA or +/- 20 uA. The code will know what the mode is so changes the output data type, -10 -> +10 to the correct form.
    char error_msg[FUGNTN_MSG_SIZE];
    memset(error,'\0', FUGNTN_MSG_SIZE);

    sprintf(msg, ">S1 %f \r\n",current);

    if(FUGNTN_write(serial_port, msg, error_msg)){sprintf(error,"%s",error_msg);return FUGNTN_FAILURE;}

    if(FUGNTN_read(serial_port, reply, error_msg)){sprintf(error,"%s",error_msg);return FUGNTN_FAILURE;}

    return FUGNTN_SUCCESS;
}


int FUGNTN_set_voltage(const int serial_port, int voltage, char* error)
{
    char msg[FUGNTN_MSG_SIZE];
    char reply[FUGNTN_MSG_SIZE];
    char error_msg[FUGNTN_MSG_SIZE];
    memset(error,'\0', FUGNTN_MSG_SIZE);
    sprintf(msg, ">S0 %i \r\n", voltage); //the data type here is 1 = 1 V.

    if(FUGNTN_write(serial_port, msg, error_msg)){sprintf(error,"%s",error_msg);return FUGNTN_FAILURE;}

    if(FUGNTN_read(serial_port, reply, error_msg)){sprintf(error,"%s",error_msg);return FUGNTN_FAILURE;}

    return FUGNTN_SUCCESS;
}

int FUGNTN_measure_current(const int serial_port, float* current, char* error)
{
    char error_msg[FUGNTN_MSG_SIZE];
    char reply[FUGNTN_MSG_SIZE];
    memset(error, '\0', FUGNTN_MSG_SIZE);

    if(FUGNTN_write(serial_port, ">M1? \r\n", error_msg)){sprintf(error,"%s",error_msg);return FUGNTN_FAILURE;}//reads the monitor current, this is constantly updated value. Returns the actual current for given mode.

    if(FUGNTN_read(serial_port, reply, error_msg)){sprintf(error,"%s",error_msg);return FUGNTN_FAILURE;}

	if(reply[0] == 'M' && reply[1] == '1')
	{
    	sscanf(reply, "%*c%*c%*c%f\n", current);
    }
	else{*current = 0; sprintf(error,"READ ERROR");return FUGNTN_FAILURE;}

    return FUGNTN_SUCCESS;
}

int FUGNTN_measure_voltage(const int serial_port, float* voltage, char* error)
{
    char error_msg[FUGNTN_MSG_SIZE];
    char reply[FUGNTN_MSG_SIZE];
    memset(error, '\0', FUGNTN_MSG_SIZE);

    if(FUGNTN_write(serial_port, ">M0? \r\n", error_msg)){sprintf(error,"%s",error_msg);return FUGNTN_FAILURE;}//reads the monitor current, this is constantly updated value. Returns the actual current for given mode.

    if(FUGNTN_read(serial_port, reply, error_msg)){sprintf(error,"%s",error_msg);return FUGNTN_FAILURE;}

	if(reply[0] == 'M' && reply[1] == '0')
	{
    	sscanf(reply, "%*c%*c%*c%f\n", voltage);
	}
	else{*voltage = 0; sprintf(error,"READ ERROR");return FUGNTN_FAILURE;}

    return FUGNTN_SUCCESS;
}

int FUGNTN_get_voltage(const int serial_port, float* voltage, char* error)
{
    char error_msg[FUGNTN_MSG_SIZE];
    char reply[FUGNTN_MSG_SIZE];
    memset(error, '\0', FUGNTN_MSG_SIZE);

    if(FUGNTN_write(serial_port,  ">S0? \r\n", error_msg)){sprintf(error,"%s",error_msg);return FUGNTN_FAILURE;}//reads the monitor current, this is constantly updated value. Returns the actual current for given mode.

    if(FUGNTN_read(serial_port, reply, error_msg)){sprintf(error,"%s",error_msg);return FUGNTN_FAILURE;}

	if(reply[0] == 'S' && reply[1] == '0')
	{
    	sscanf(reply, "%*c%*c%*c%f\n", voltage);
	}
	else{*voltage = 0; sprintf(error,"READ ERROR");return FUGNTN_FAILURE;}

    return FUGNTN_SUCCESS;
}

int FUGNTN_get_current(const int serial_port, float* current, char* error)
{
    char error_msg[FUGNTN_MSG_SIZE];
    char reply[FUGNTN_MSG_SIZE];
    memset(error, '\0', FUGNTN_MSG_SIZE);

    if(FUGNTN_write(serial_port,  ">S1? \r\n", error_msg)){sprintf(error,"%s",error_msg);return FUGNTN_FAILURE;}//reads the monitor current, this is constantly updated value. Returns the actual current for given mode.

    if(FUGNTN_read(serial_port, reply, error_msg)){sprintf(error,"%s",error_msg);return FUGNTN_FAILURE;}

	if(reply[0] == 'S' && reply[1] == '1')
	{
    	sscanf(reply, "%*c%*c%*c%f\n", current);
	}
	else{*current = 0; sprintf(error,"READ ERROR");return FUGNTN_FAILURE;}

    return FUGNTN_SUCCESS;
}
