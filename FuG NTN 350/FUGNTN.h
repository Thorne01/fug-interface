#ifndef __FUGNTN__
#define __FUGNTN__

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <fcntl.h>
#include <termios.h>
#include <unistd.h>
#include <errno.h>
#include <sys/ioctl.h>
#include <sys/stat.h>
#include "math.h"
#include <time.h>

#include <sys/uio.h>
#include <sys/socket.h>

#define FUGNTN_MSG_SIZE 512
#define FUGNTN_FAILURE 1
#define FUGNTN_SUCCESS 0
#define FUGNTN_CONNECT_FAIL -1

int FUGNTN_error_handle(int error_code, char* err_msg);

int FUGNTN_connect(const char* port_id, char* error);

int FUGNTN_configure(const int serial_port, char* error);

int FUGNTN_write(const int serial_port, char* msg, char* error);

int FUGNTN_read(const int serial_port, char* reply, char* error);

int FUGNTN_disconnect(const int serial_port);

int FUGNTN_set_reset(int serial_port, char* error);

int FUGNTN_get_identify(const int serial_port, char* reply, char* error);

int FUGNTN_get_SELD(const int serial_port, char* error);

int FUGNTN_set_HV_output(const int serial_port, int status, char* error);

int FUGNTN_get_output_state(const int serial_port, int* status, char* error);

int FUGNTN_set_current_res(const int serial_port, int arg, char* error);

int FUGNTN_set_voltage_res(const int serial_port, int arg, char* error);

int FUGNTN_set_current(const int serial_port, double current, char* error);

int FUGNTN_set_voltage(const int serial_port, int voltage, char* error);

int FUGNTN_measure_current(const int serial_port, float* current, char* error);

int FUGNTN_measure_voltage(const int serial_port, float* voltage, char* error);

int FUGNTN_get_voltage(const int serial_port, float* voltage, char* error);

int FUGNTN_get_current(const int serial_port, float* current, char* error);

#endif
