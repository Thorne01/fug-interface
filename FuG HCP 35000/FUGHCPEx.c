#include <stdio.h>
#include <string.h>

#include "FUGHCP.h"

void flush_stdin(void);
int enter_press();

int main(void)
{
    char FUGHCP_err[FUGHCP_MSG_SIZE];
    char id[FUGHCP_MSG_SIZE];
    int output_state;

    int fd = FUGHCP_connect("/dev/serial/by-id/usb-MOXA_Technologies_MOXA_UPort_1610-8-if00-port3", FUGHCP_err);
    if(fd == -1){printf("%s\n",FUGHCP_err); return -1;}

    if(FUGHCP_get_identify(fd, id, FUGHCP_err)){printf("%s\n",FUGHCP_err);return -1;}
    else{printf("Device ID: %s\n",id);}

    //FILE *fptr;

    //fptr = fopen("/home/neutron/Example_test.txt","a+");

    //if(fptr == NULL)
    //{
    //    printf("Error opening file!");
    //    exit(1);
    //}

    //if(FUGHCP_get_SELD(fd, FUGHCP_err)){printf("%s\n",FUGHCP_err);return -1;}

    if(FUGHCP_set_reset(fd,FUGHCP_err)){printf("%s\n",FUGHCP_err);return -1;}

    //if(FUGHCP_set_interlock(fd, 1, FUGHCP_err)){printf("%s\n",FUGHCP_err);return -1;}

    //sleep(5);

	if(FUGHCP_set_polarity(fd, 1, FUGHCP_err)){printf("%s\n",FUGHCP_err);return -1;}

	sleep(5);

    if(FUGHCP_set_HV_output(fd, 1,FUGHCP_err)){printf("%s\n",FUGHCP_err);return -1;}

    if(FUGHCP_get_output_state(fd, &output_state, FUGHCP_err)){printf("%s\n",FUGHCP_err);return -1;}
    printf("Device output state: %d\n", output_state);
    if(output_state != 1){printf("FUGHCB: CANNOT ENGAGE OUTPUT!");return -1;}//error handle to check if interlock is open or not

    //if(FUGHCP_set_current_res(fd, 5,FUGHCP_err)){printf("%s\n",FUGHCP_err);return -1;} //int 0-7, zero is 14 bit+sign, typ. 5 18 bit +sign

    //if(FUGHCP_set_voltage_res(fd, 5,FUGHCP_err)){printf("%s\n",FUGHCP_err);return -1;}

    //if(FUGHCP_set_current(fd, 0.00001,FUGHCP_err)){printf("%s\n",FUGHCP_err);return -1;} //current limit, absolute value

	char cal_state[FUGHCP_MSG_SIZE];
	if(FUGHCP_get_cal_lock(fd, cal_state, FUGHCP_err)){printf("%s\n",FUGHCP_err);return -1;}
	printf("%s\n",cal_state);

    if(FUGHCP_set_voltage(fd, 10000,FUGHCP_err)){printf("%s\n",FUGHCP_err);return -1;} //voltage in V not kV! This sets the limit to ramp up to

    if(FUGHCP_set_ramp_mode(fd, 2, FUGHCP_err)){printf("%s\n",FUGHCP_err);return -1;}

    if(FUGHCP_set_ramp(fd, 100, FUGHCP_err)){printf("%s\n",FUGHCP_err);return -1;}

    char reply_mode[FUGHCP_MSG_SIZE];
    if(FUGHCP_get_ramp_mode(fd, reply_mode, FUGHCP_err)){printf("%s\n",FUGHCP_err);return -1;}
    printf("ramp mode: %s\n",reply_mode);

    char reply_rate[FUGHCP_MSG_SIZE];
    if(FUGHCP_get_ramp_rate(fd, reply_rate, FUGHCP_err)){printf("%s\n",FUGHCP_err);return -1;}
    printf("ramp rate: %s\n",reply_rate);

    float curr_lim;
    if(FUGHCP_get_current(fd, &curr_lim,FUGHCP_err)){printf("%s\n",FUGHCP_err);return -1;}
    else{
        printf("Current limit:%f (uA)\n",curr_lim);}

    float volt_lim;
    if(FUGHCP_get_voltage(fd, &volt_lim,FUGHCP_err)){printf("%s\n",FUGHCP_err);return -1;}
    else{
        printf("Voltage limit:%f (V)\n",volt_lim);}

    float curr;
    float volt;
    int i = 0;
    printf("\nMeasurement Step\tVoltage (V)\tCurrent (uA)\n");

	if(FUGHCP_set_polarity(fd, 0, FUGHCP_err)){printf("%s\n",FUGHCP_err);return -1;} //check its positive

	//sleep(5);

	//if(FUGHCP_set_polarity(fd, 1, FUGHCP_err)){printf("%s\n",FUGHCP_err);return -1;} //check its negative

	//sleep(10);
	//printf("switching to negative");

    //fprintf(fptr, "\nMeasurement Step\tVoltage (V)\tCurrent (uA)\n");
    while(!enter_press())
    {
        if(FUGHCP_measure_current(fd, &curr,FUGHCP_err)){printf("%s\n",FUGHCP_err);return -1;}

        if(FUGHCP_measure_voltage(fd, &volt,FUGHCP_err)){printf("%s\n",FUGHCP_err);return -1;}

        printf("%d\t%f\t%f\n",i, volt, curr);
        //fprintf(fptr,"%d\t%f\t%f\n",i, volt, curr);
        i++;
    }

    if(FUGHCP_set_HV_output(fd, 0,FUGHCP_err)){printf("%s\n",FUGHCP_err);return -1;}

	//file never closed

    FUGHCP_disconnect(fd);
}

void flush_stdin(void)
{
    int c;
    do {
        c = getchar();
    } while (c != '\n' && c != EOF);
}

int enter_press()
{
    int stdin_value = 0;
    struct timeval tv;
    fd_set fds;
    tv.tv_sec = 0;
    tv.tv_usec = 0;
    FD_ZERO(&fds);
    FD_SET(STDIN_FILENO, &fds); //STDIN_FILENO is 0
    select(STDIN_FILENO+1, &fds, NULL, NULL, &tv);
    stdin_value = FD_ISSET(STDIN_FILENO, &fds);
    if (stdin_value != 0)
        flush_stdin();
    return stdin_value;
}
