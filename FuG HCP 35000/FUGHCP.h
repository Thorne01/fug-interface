#ifndef __FUGHCP__
#define __FUGHCP__

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <fcntl.h>
#include <termios.h>
#include <unistd.h>
#include <errno.h>
#include <sys/ioctl.h>
#include <sys/stat.h>
#include "math.h"
#include <time.h>

#include <sys/uio.h>
#include <sys/socket.h>

#define FUGHCP_MSG_SIZE 512
#define FUGHCP_FAILURE 1
#define FUGHCP_SUCCESS 0
#define FUGHCP_CONNECT_FAIL -1

int FUGHCP_error_handle(int error_code, char* err_msg);

int FUGHCP_connect(const char* port_id, char* error);

int FUGHCP_configure(const int serial_port, char* error);

int FUGHCP_write(const int serial_port, char* msg, char* error);

int FUGHCP_read(const int serial_port, char* reply, char* error);

int FUGHCP_disconnect(const int serial_port);

int FUGHCP_set_reset(int serial_port, char* error);

int FUGHCP_set_interlock(const int serial_port, int status, char* error);

int FUGHCP_get_identify(const int serial_port, char* reply, char* error);

int FUGHCP_set_HV_output(const int serial_port, int status, char* error);

int FUGHCP_get_output_state(const int serial_port, int* status, char* error);

int FUGHCP_set_polarity(int serial_port, int polarity, char* error);

int FUGHCP_set_current_res(const int serial_port, int arg, char* error);

int FUGHCP_set_voltage_res(const int serial_port, int arg, char* error);

int FUGHCP_set_current(const int serial_port, double current, char* error);

int FUGHCP_set_voltage(const int serial_port, int voltage, char* error);

int FUGHCP_measure_current(const int serial_port, float* current, char* error);

int FUGHCP_measure_voltage(const int serial_port, float* voltage, char* error);

int FUGHCP_get_voltage(const int serial_port, float* voltage, char* error);

int FUGHCP_get_current(const int serial_port, float* current, char* error);

int FUGHCP_set_ramp(const int serial_port, unsigned int voltage, char* error);

int FUGHCP_set_ramp_mode(const int serial_port, int mode, char* error);

int FUGHCP_get_ramp_mode(const int serial_port, char* reply, char* error);

int FUGHCP_get_ramp_rate(const int serial_port, char* reply, char* error);

int FUGHCP_get_cal_lock(const int serial_port, char* state, char* error);

#endif
