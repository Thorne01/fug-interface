/******************************************************************************
* @author J.Thorne
*********************************************************************************/

#include "FUGHCP.h"

int FUGHCP_error_handle(int error_code, char* err_msg)
{
    char error_prefix[] = "FUGHCP: "; //define prefix of the return for the error
    switch(error_code)
    {
        case 0 :
            break;
        case 1 :
            sprintf(err_msg, "%sNO DATA AVAILABLE",error_prefix);
            break;
        case 2 :
            sprintf(err_msg, "%sUNKNOWN REGISTER TYPE",error_prefix);
            break;
        case 4 :
            sprintf(err_msg, "%sINVALID ARGUMENT",error_prefix);
            break;
        case 5 :
            sprintf(err_msg, "%sARGUMENT OUT OF RANGE",error_prefix);
            break;
        case 6 :
            sprintf(err_msg, "%sREGISTER IS READ-ONLY",error_prefix);
            break;
        case 7 :
            sprintf(err_msg, "%sRECEIVE OVERFLOW",error_prefix);
            break;
        case 8 :
            sprintf(err_msg, "%sEEPROM IS WRITE PROTECTED",error_prefix);
            break;
        case 9 :
            sprintf(err_msg, "%sADDRESS ERROR",error_prefix);
            break;
        case 10 :
            sprintf(err_msg, "%sUNKNOWN COMMAND",error_prefix);
            break;
        case 11 :
            sprintf(err_msg, "%sNOT ALLOWED TRIGGER-ON-TALK",error_prefix);
            break;
        case 12 :
            sprintf(err_msg, "%sINVALID ARGUMENT IN ~Tn COMMAND",error_prefix);
            break;
        case 13 :
            sprintf(err_msg, "%sINVALID N-VALUE",error_prefix);
            break;
        case 14 :
            sprintf(err_msg, "%sREGISTER IS WRITE ONLY",error_prefix);
            break;
        case 15 :
            sprintf(err_msg, "%sSTRING TOO LONG",error_prefix);
            break;
        case 16 :
            sprintf(err_msg, "%sWRONG CHECKSUM",error_prefix);
            break;
        case 17 :
            sprintf(err_msg, "%sINTERLOCK OPEN?",error_prefix);
            break;
        case 18 :
            sprintf(err_msg, "%sDIGITAL PROGRAMMING NOT ACTIVE",error_prefix);
            break;
        case 19 :
            sprintf(err_msg, "%sERROR WRITING TO THE DEVICE",error_prefix);
            break;
        default :
            sprintf(err_msg, "%sUNKNOWN ERROR CODE",error_prefix);
    }
    return FUGHCP_SUCCESS;
}
//list of codes and meanings

int FUGHCP_connect(const char* port_id, char* error)
{
    memset(error, '\0', FUGHCP_MSG_SIZE * sizeof(char));
    int serial_port = open(port_id, O_RDWR | O_NOCTTY | O_SYNC);
    if (serial_port == -1)
    {
        sprintf(error, "FUGHCP: ERROR OPENING DEVICE");
        return FUGHCP_CONNECT_FAIL;
    } else {
        if (FUGHCP_configure(serial_port, error)) {
            tcflush(serial_port, TCIOFLUSH);
            return FUGHCP_CONNECT_FAIL;
        }
    }
    return serial_port;
}

int FUGHCP_configure(const int serial_port, char* error){

    struct termios options;
    memset(&options, 0, sizeof(options));  /* clear the new struct */
    memset(error, 0, FUGHCP_MSG_SIZE*sizeof(char));

    cfsetispeed(&options, B9600); //baud rate
    cfsetospeed(&options, B9600);
    options.c_iflag &= ~(IGNBRK | BRKINT | ICRNL | INLCR | PARMRK | INPCK | ISTRIP | IXON);
    options.c_oflag = 0;
    options.c_lflag &= ~(ECHO | ECHONL | IEXTEN );
    options.c_lflag |= ICANON | ISIG;
    options.c_cflag &= ~(CSIZE | PARENB);
    options.c_cflag |= CS8;
    options.c_cflag |= CLOCAL | CREAD;

    options.c_cc[VMIN]  = 0;
    options.c_cc[VTIME] = 0;
    options.c_cc[VEOL] = 0;
    options.c_cc[VEOL2] = 0;
    options.c_cc[VEOF] = 0x04;


    if(tcsetattr(serial_port, TCSANOW, &options) != 0)
    {
        sprintf(error, "FUGHCP: FAILURE TO CONFIGURE TERMIOS");
        return FUGHCP_FAILURE;
    }
    else
    {
        return FUGHCP_SUCCESS;
    }
}

int FUGHCP_write(const int serial_port, char* msg, char* error)
{
    unsigned int status = 0x00;
    ioctl(serial_port, TIOCMGET, &status);//define type of message
    memset(error, '\0', FUGHCP_MSG_SIZE);
    int nbytes_written = write(serial_port, msg, strlen(msg)+1);//builds the message to be sent

    if(nbytes_written != strlen(msg)+1)
    {FUGHCP_error_handle(19, error);
        return FUGHCP_FAILURE;} //error check
    else
    {return FUGHCP_SUCCESS;}
}

int FUGHCP_read(const int serial_port, char* reply, char* error)
{
    memset(reply, '\0', FUGHCP_MSG_SIZE* sizeof(char));

    int bytes_available = 0;
    long msec = 0, trigger = 5000; /* 5s to wait before timeout */
    clock_t before = clock();
    while(bytes_available<1)
    {
        clock_t difference = clock() - before;
        msec = difference * 1000 / CLOCKS_PER_SEC; //50-60 msec delay for communication
        ioctl(serial_port, FIONREAD, &bytes_available);
        if(msec > trigger)
        {
            sprintf(error, "FUGHCP: READ TIMEOUT");
            return FUGHCP_FAILURE;
        }
    }
    read(serial_port, reply, bytes_available);

    if(reply[0] == 'E' && reply[1] != '0')//if message is accepted, will return E0. However, some queries can reply with M1:## so this is taken into account. Any error from the PS will return E1-16.
    {
        int ret_error_code;
        unsigned int msg_length = strlen(reply);
        char char_error_code[msg_length];
        memcpy(char_error_code, reply+1, msg_length);
        sscanf(char_error_code, "%d", &ret_error_code);
        FUGHCP_error_handle(ret_error_code, error);
        return FUGHCP_FAILURE;
    }
    else
    {return FUGHCP_SUCCESS;}
}

int FUGHCP_disconnect(const int serial_port)
{
	return close(serial_port);
}

int FUGHCP_set_reset(int serial_port, char* error)
{
    char reply[FUGHCP_MSG_SIZE];
    char error_msg[FUGHCP_MSG_SIZE];
    memset(error, '\0', FUGHCP_MSG_SIZE *sizeof(char));

    if(FUGHCP_write(serial_port, "= \r\n", error_msg)){sprintf(error,"%s",error_msg);return FUGHCP_FAILURE;}

    if(FUGHCP_read(serial_port, reply, error_msg)){sprintf(error,"%s",error_msg);return FUGHCP_FAILURE;}

    return FUGHCP_SUCCESS;
}

int FUGHCP_get_identify(const int serial_port, char* reply, char* error)
{
    char error_msg[FUGHCP_MSG_SIZE];
    memset(reply, '\0', FUGHCP_MSG_SIZE* sizeof(char));
    memset(error, '\0', FUGHCP_MSG_SIZE* sizeof(char));

    if(FUGHCP_write(serial_port, "*IDN? \r\n", error_msg)){sprintf(error,"%s",error_msg);return FUGHCP_FAILURE;}

    if(FUGHCP_read(serial_port, reply, error_msg)){sprintf(error,"%s",error_msg);return FUGHCP_FAILURE;}

    return FUGHCP_SUCCESS;
}

int FUGHCP_get_SELD(const int serial_port, char* error)
{
    char reply[FUGHCP_MSG_SIZE];
    char error_msg[FUGHCP_MSG_SIZE];
    memset(error, '\0', FUGHCP_MSG_SIZE* sizeof(char));

    if(FUGHCP_write(serial_port, ">DSD? \r\n", error_msg)){sprintf(error,"%s",error_msg);return FUGHCP_FAILURE;}
    if(FUGHCP_read(serial_port, reply, error_msg)){sprintf(error,"%s",error_msg);return FUGHCP_FAILURE;}

    if(reply[4] != '1'){FUGHCP_error_handle(18, error);return FUGHCP_FAILURE;}//will return special error if there is an issue
    return FUGHCP_SUCCESS;
}

int FUGHCP_set_HV_output(const int serial_port, int status, char* error)
{
    char msg[FUGHCP_MSG_SIZE];
    char reply[FUGHCP_MSG_SIZE];
    char error_msg[FUGHCP_MSG_SIZE];
    memset(error,'\0', FUGHCP_MSG_SIZE);
    sprintf(msg, ">BON %i \r\n", status); //BON command has to be set with B2. BON turns the output on and off.

    if(FUGHCP_write(serial_port, msg, error_msg)){sprintf(error,"%s",error_msg);return FUGHCP_FAILURE;}//status = 0 is off

    if(FUGHCP_read(serial_port, reply, error_msg)){sprintf(error,"%s",error_msg);return FUGHCP_FAILURE;}

    return FUGHCP_SUCCESS;
}

int FUGHCP_get_output_state(const int serial_port, int* status, char* error)
{
    char reply[FUGHCP_MSG_SIZE];
    char error_msg[FUGHCP_MSG_SIZE];
    memset(error, '\0', FUGHCP_MSG_SIZE* sizeof(char));

    if(FUGHCP_write(serial_port,  ">DON? \r\n", error_msg)){sprintf(error,"%s",error_msg);return FUGHCP_FAILURE;}//DON queries status of the BON

    if(FUGHCP_read(serial_port, reply, error_msg)){sprintf(error,"%s",error_msg);return FUGHCP_FAILURE;}

    if(reply[4] != '1'){*status = 0;}
    else{*status = 1;}

    return FUGHCP_SUCCESS;
}

int FUGHCP_set_polarity(int serial_port, int polarity, char* error) //sets polarity of the power supply, B0 0 is positive, B0 1 is negative
{
    char reply[FUGHCP_MSG_SIZE];
    char error_msg[FUGHCP_MSG_SIZE];
	char msg[FUGHCP_MSG_SIZE];
    memset(error, '\0', FUGHCP_MSG_SIZE *sizeof(char));
	sprintf(msg, "p%i \r\n", polarity);

	//check that the polarity is a bool

    if(FUGHCP_write(serial_port, msg, error_msg)){sprintf(error,"%s",error_msg);return FUGHCP_FAILURE;}

    if(FUGHCP_read(serial_port, reply, error_msg)){sprintf(error,"%s",error_msg);return FUGHCP_FAILURE;}

    return FUGHCP_SUCCESS;
}

int FUGHCP_set_current_res(const int serial_port, int arg, char* error)
{
    char msg[FUGHCP_MSG_SIZE];
    char reply[FUGHCP_MSG_SIZE];
    char error_msg[FUGHCP_MSG_SIZE];
    memset(error,'\0', FUGHCP_MSG_SIZE);
    sprintf(msg, ">M1I %i \r\n", arg);//sets the resolution and integration time of the current read out. Arg can be set 0-7 for required number of bits, see FUG manual. Default is 5 (18 bit +sign, integration time 80ms).

    if(FUGHCP_write(serial_port, msg, error_msg)){sprintf(error,"%s",error_msg);return FUGHCP_FAILURE;}//status = 0 is off

    if(FUGHCP_read(serial_port, reply, error_msg)){sprintf(error,"%s",error_msg);return FUGHCP_FAILURE;}

    return FUGHCP_SUCCESS;
}

int FUGHCP_set_voltage_res(const int serial_port, int arg, char* error)
{
    char msg[FUGHCP_MSG_SIZE];
    char reply[FUGHCP_MSG_SIZE];
    char error_msg[FUGHCP_MSG_SIZE];
    memset(error,'\0', FUGHCP_MSG_SIZE);
    sprintf(msg, ">M0I %i \r\n", arg);//sets resolution of the voltage read out

    if(FUGHCP_write(serial_port, msg, error_msg)){sprintf(error,"%s",error_msg);return FUGHCP_FAILURE;}

    if(FUGHCP_read(serial_port, reply, error_msg)){sprintf(error,"%s",error_msg);return FUGHCP_FAILURE;}

    return FUGHCP_SUCCESS;
}

int FUGHCP_set_current(const int serial_port, double current, char* error)
{
    char msg[FUGHCP_MSG_SIZE];
    char reply[FUGHCP_MSG_SIZE];//sets the current input by the user. The user can put in any value for the given current range, either +/-200 uA or +/- 20 uA. The code will know what the mode is so changes the output data type, -10 -> +10 to the correct form.
    char error_msg[FUGHCP_MSG_SIZE];
    memset(error,'\0', FUGHCP_MSG_SIZE);

    sprintf(msg, ">S1 %f \r\n",current);

    if(FUGHCP_write(serial_port, msg, error_msg)){sprintf(error,"%s",error_msg);return FUGHCP_FAILURE;}

    if(FUGHCP_read(serial_port, reply, error_msg)){sprintf(error,"%s",error_msg);return FUGHCP_FAILURE;}

    return FUGHCP_SUCCESS;
}


int FUGHCP_set_voltage(const int serial_port, int voltage, char* error)
{
    char msg[FUGHCP_MSG_SIZE];
    char reply[FUGHCP_MSG_SIZE];
    char error_msg[FUGHCP_MSG_SIZE];
    memset(error,'\0', FUGHCP_MSG_SIZE);
    sprintf(msg, ">S0 %i \r\n", voltage); //the data type here is 1 = 1 V.

    if(FUGHCP_write(serial_port, msg, error_msg)){sprintf(error,"%s",error_msg);return FUGHCP_FAILURE;}

    if(FUGHCP_read(serial_port, reply, error_msg)){sprintf(error,"%s",error_msg);return FUGHCP_FAILURE;}

    return FUGHCP_SUCCESS;
}

int FUGHCP_measure_current(const int serial_port, float* current, char* error)
{
    char error_msg[FUGHCP_MSG_SIZE];
    char reply[FUGHCP_MSG_SIZE];
    memset(error, '\0', FUGHCP_MSG_SIZE);

    if(FUGHCP_write(serial_port, ">M1? \r\n", error_msg)){sprintf(error,"%s",error_msg);return FUGHCP_FAILURE;}//reads the monitor current, this is constantly updated value. Returns the actual current for given mode.

    if(FUGHCP_read(serial_port, reply, error_msg)){sprintf(error,"%s",error_msg);return FUGHCP_FAILURE;}

    sscanf(reply, "%*c%*c%*c%f\n", current);
    
    return FUGHCP_SUCCESS;
}

int FUGHCP_measure_voltage(const int serial_port, float* voltage, char* error)
{
    char error_msg[FUGHCP_MSG_SIZE];
    char reply[FUGHCP_MSG_SIZE];
    memset(error, '\0', FUGHCP_MSG_SIZE);

    if(FUGHCP_write(serial_port, ">M0? \r\n", error_msg)){sprintf(error,"%s",error_msg);return FUGHCP_FAILURE;}//reads the monitor current, this is constantly updated value. Returns the actual current for given mode.

    if(FUGHCP_read(serial_port, reply, error_msg)){sprintf(error,"%s",error_msg);return FUGHCP_FAILURE;}

    sscanf(reply, "%*c%*c%*c%f\n", voltage);
    return FUGHCP_SUCCESS;
}

int FUGHCP_get_voltage(const int serial_port, float* voltage, char* error)
{
    char error_msg[FUGHCP_MSG_SIZE];
    char reply[FUGHCP_MSG_SIZE];
    memset(error, '\0', FUGHCP_MSG_SIZE);

    if(FUGHCP_write(serial_port,  ">S0? \r\n", error_msg)){sprintf(error,"%s",error_msg);return FUGHCP_FAILURE;}//reads the monitor current, this is constantly updated value. Returns the actual current for given mode.

    if(FUGHCP_read(serial_port, reply, error_msg)){sprintf(error,"%s",error_msg);return FUGHCP_FAILURE;}

    sscanf(reply, "%*c%*c%*c%f\n", voltage);
    return FUGHCP_SUCCESS;
}

int FUGHCP_get_current(const int serial_port, float* current, char* error)
{
    char error_msg[FUGHCP_MSG_SIZE];
    char reply[FUGHCP_MSG_SIZE];
    memset(error, '\0', FUGHCP_MSG_SIZE);

    if(FUGHCP_write(serial_port,  ">S1? \r\n", error_msg)){sprintf(error,"%s",error_msg);return FUGHCP_FAILURE;}//reads the monitor current, this is constantly updated value. Returns the actual current for given mode.

    if(FUGHCP_read(serial_port, reply, error_msg)){sprintf(error,"%s",error_msg);return FUGHCP_FAILURE;}

    sscanf(reply, "%*c%*c%*c%f\n", current);

    return FUGHCP_SUCCESS;
}

int FUGHCP_set_ramp_mode(const int serial_port, int mode, char* error)
{
    char msg[FUGHCP_MSG_SIZE];
    char reply[FUGHCP_MSG_SIZE];
    char error_msg[FUGHCP_MSG_SIZE];
    memset(error,'\0', FUGHCP_MSG_SIZE);
    sprintf(msg, ">S0B %i \r\n", mode);//ramp function comes before the set ramp rate. This defines the mode type, details given in manual. Default for our needs is to set this to 2.

    if(FUGHCP_write(serial_port, msg, error_msg)){sprintf(error,"%s",error_msg);return FUGHCP_FAILURE;}

    if(FUGHCP_read(serial_port, reply, error_msg)){sprintf(error,"%s",error_msg);return FUGHCP_FAILURE;}

    return FUGHCP_SUCCESS;
}

int FUGHCP_set_ramp(const int serial_port, unsigned int voltage, char* error)
{
    char msg[FUGHCP_MSG_SIZE];
    char reply[FUGHCP_MSG_SIZE];
    char error_msg[FUGHCP_MSG_SIZE];
    memset(error,'\0', FUGHCP_MSG_SIZE);
    sprintf(msg, ">S0R %i \r\n", voltage); //defines the rate, i.e. the number of V/sec. This cannot be larger then 1 kV/sec.

    if(FUGHCP_write(serial_port, msg, error_msg)){sprintf(error,"%s",error_msg);return FUGHCP_FAILURE;}

    if(FUGHCP_read(serial_port, reply, error_msg)){sprintf(error,"%s",error_msg);return FUGHCP_FAILURE;}

    return FUGHCP_SUCCESS;
}

int FUGHCP_get_ramp_mode(const int serial_port, char* reply, char* error)
{
    char error_msg[FUGHCP_MSG_SIZE];
    memset(error, '\0', FUGHCP_MSG_SIZE);

    if(FUGHCP_write(serial_port,  ">S1S? \r\n", error_msg)){sprintf(error,"%s",error_msg);return FUGHCP_FAILURE;}

    if(FUGHCP_read(serial_port, reply, error_msg)){sprintf(error,"%s",error_msg);return FUGHCP_FAILURE;}

    return FUGHCP_SUCCESS;
}

int FUGHCP_get_ramp_rate(const int serial_port, char* reply, char* error)
{
    char error_msg[FUGHCP_MSG_SIZE];
    memset(error, '\0', FUGHCP_MSG_SIZE);

    if(FUGHCP_write(serial_port,  ">S1R? \r\n", error_msg)){sprintf(error,"%s",error_msg);return FUGHCP_FAILURE;}

    if(FUGHCP_read(serial_port, reply, error_msg)){sprintf(error,"%s",error_msg);return FUGHCP_FAILURE;}

    return FUGHCP_SUCCESS;
}

int FUGHCP_set_interlock(const int serial_port, int status, char* error)
{
    char msg[FUGHCP_MSG_SIZE];
    char reply[FUGHCP_MSG_SIZE];
    char error_msg[FUGHCP_MSG_SIZE];
    memset(error,'\0', FUGHCP_MSG_SIZE);

    sprintf(msg, ">B2 %i \r\n", status); //B2 command queries the interlock system

    if(FUGHCP_write(serial_port, msg, error_msg)){sprintf(error,"%s",error_msg);return FUGHCP_FAILURE;}

    if(FUGHCP_read(serial_port, reply, error_msg)){sprintf(error,"%s",error_msg);return FUGHCP_FAILURE;}

    return FUGHCP_SUCCESS;
}

int FUGHCP_get_cal_lock(const int serial_port, char* state, char* error)
{
    char reply[FUGHCP_MSG_SIZE];
    char error_msg[FUGHCP_MSG_SIZE];
    memset(error, '\0', FUGHCP_MSG_SIZE* sizeof(char));

    if(FUGHCP_write(serial_port,  ">DCAL? \r\n", error_msg)){sprintf(error,"%s",error_msg);return FUGHCP_FAILURE;}

    if(FUGHCP_read(serial_port, state, error_msg)){sprintf(error,"%s",error_msg);return FUGHCP_FAILURE;}

    return FUGHCP_SUCCESS;
}
